<h1>Meeks-PRFound</h1>

<p>A reference implementation for counting votes with Meek's method STV.</p>

<h1>Table of Contents</h1>

<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#version">Version</a></li>
<li><a href="#repository-structure">Repository Structure</a></li>
<li><a href="#use-examples">Use examples</a></li>
<li><a href="#testing">Testing</a></li>
<li><a href="#limitations">Limitations</a></li>
<li><a href="#extensions">Extensions</a></li>
<li><a href="#licensing">Licensing</a></li>
</ul>

<h2>Introduction <a id="introduction"></a></h2>

<p>This is a basic implementation in Python of the vote counting
algorithm for ranked choice voting (RCV) / single transferable vote
(STV) as specified by the Proportional Representation Foundation with
its <a href="
https://prfound.org/resources/reference/reference-meek-rule/">reference rule for Meek's method</a>.</p>

<p>This implementation focuses on the core process of counting votes with
the intent that other software can be built around it to accommodate
specific ballot formats and results reporting.  The included test cases
can be used as a starting point for conformance testing of other
implementations of the vote counting.</p>

<p>Meek's method is a form of STV that can provide a fairer and more
comprehensive transfer of surplus than other forms of STV, including
variations of the weighted inclusive Gregory method (WIGM) which also
involves transfers of surplus as fractions of a vote.  While any form of
STV tends to greatly reduce the practical opportunities for tactical
voting, Meek's method precludes some of the opportunities that still
exist with other versions of STV.  However the tradeoff is that Meek's
method is more computationally intensive, to the extent that it is
typically not practical to count votes with Meek's method without the
assistance of a computer for contests with more than a few winners.</p>

<p>Meek's method can require retabulating votes from all ballots for each
of multiple iterations per round.  This is because it allows surplus to
be distributed each round from and to newly elected winners and
previously elected winners.  This creates a network of flows of surplus
between candidates that for an individual ballot is strictly sequential
but at the aggregate level can involve bidirectional flows and loops.</p>

<p>The optimum level for the winning quota that distributes all surplus
votes to hopeful candidates (those that have not yet been elected or
defeated) and leaves all candidates elected so far with exactly a quota
of votes is described by a system of non-linear, multivariate polynomial
equations that are linear in each variable, one variable for each
candidate elected so far.  In each round, Meek's method uses an
iterative search method to find an approximation to the unique solution
for the optimum quota based on that system of equations, with each
iteration involving a tabulation of votes for all ballots.</p>

<p>The reference rule published by the Proportional Representation
Foundation is particularly significant because it specifies a process
for doing that iterative search in a way that will produce the same
result across otherwise independent but conforming implementations of
the rule, provided ties are resolved consistently.</p>

<h2>Version <a id="version"></a></h2>

<p>This is version 3.0.0 of the Meeks-PRFound project.  Version 3 continues
to introduce extensions and has default behavior that does not
strictly conform to the reference rule. However options are included
to allow tabulations that still do conform with the reference rule.</p>

<p>Version 3.0 introduces support to exclude and protect candidates.  An
excluded candidate is not eligible to be elected or receive votes and is
marked as defeated before the first round of tabulation.  The reference
rule uses the term "withdrawn" rather than "excluded".  A protected
candidate is assured of being elected.</p>

<h2>Repository Structure <a id="repository-structure"></a></h2>

<p>The <strong><code>meeks_prfound</code></strong> directory contains a Python package with the
vote counting tabulation programs.  The <strong><code>tests</code></strong> directory contains
test programs and data that help to demonstrate both the specifics of
this particular implementation as well as conformance with the reference
rule.</p>

<p>The vote counting tabulation algorithm can be invoked with the
<strong><code>meeks_prfound.tabulate()</code></strong> function.  That function resides in the
<strong><code>meeks_prfound/meek.py</code></strong> file.  That file contains additional
documentation about the <strong><code>tabulate()</code></strong> function.</p>

<h2>Use Examples <a id="use-examples"></a></h2>

<p>Three ways to run a Meek's method STV tabulation are:</p>

<ul>
<li><a href="#caller-data">Caller gives data directly</a></li>
<li><a href="#caller-json">Caller gives data in JSON files</a></li>
<li><a href="#command-line">Command line using JSON files</a></li>
</ul>

<h3>Caller gives data directly <a id="caller-data"></a></h3>

<p>After importing the <strong><code>meek_prfound</code></strong> package with:</p>

<pre><code>import meeks_prfound
</code></pre>

<p>tabulation of RCV votes can be accomplished by calling the
<strong><code>meeks_prfound.tabulate()</code></strong> function, which is defined as:</p>

<pre><code>def tabulate(nbr_seats_to_fill, candidates, ballots,
      max_ranking_levels, tie_breaker, options={}):
</code></pre>

<p>For example:</p>

<pre><code>elected, status, tally = meeks_prfound.tabulate(1, " A B C", [
      [4, ' A B C'],
      [3, ' B A C'],
      [2, ' C B A']
      ],
      3, ' C A B', '', '', {})
</code></pre>

<p>will tabulate a contest that:</p>

<ul>
<li>produces one winner</li>
<li>has three candidates (A, B, and C)</li>
<li>has nine ballots, two of which rank C first, B second, and A third</li>
<li>allows a voter to rank up to three candidates</li>
<li>uses a tie breaker ranking for resolving ties, picking for
elimination the earliest ranked candidate of any tied candidates; so
C is picked for elimination if tied with A or B (or both) and A is
picked for elimination if tied only with B</li>
<li>no candidates are excluded from the tabulation</li>
<li>no candidates are protected from defeat and are assured of being
elected</li>
<li>does not specify any special tabulation options</li>
</ul>

<p>The above example uses a short cut for specifying a sequence of strings
by writing them in a single, delimiter-separated string with the first
character specifying what the delimiter is.</p>

<p>The result of the function is a three-tuple consisting of:</p>

<ul>
<li>elected, a set of winners</li>
<li>status, a dict showing the status of each candidate
<ul>
<li>whether the candidate was elected or defeated</li>
<li>the round in which the candidate was elected or defeated</li>
<li>the candidate's vote total when elected or defeated</li>
<li>the candidate's keep factor for the last tabulation iteration</li>
</ul></li>
<li>tally, a dict showing the essential round-by-round vote totals and
other statistics of the tabulation</li>
</ul>

<p>In this particular case, the results would be as if the following
assignments had occurred:</p>

<pre><code>elected = ('B',)
status = {
      'A': meeks_prfound.Status('A', 'defeated', 2, D(4.0), D(1.0)),
      'B': meeks_prfound.Status('B', 'elected',  2, D(5.0), D(1.0)),
      'C': meeks_prfound.Status('C', 'defeated', 1, D(2.0), D(0.0))
      }
tally = {
      'A': (D(4.0), D(4.0)),
      'B': (D(3.0), D(5.0)),
      'C': (D(2.0),),
      ':Votes for candidates': (D(9.0), D(9.0)),
      ':Overvotes': (D(0.0), D(0.0)),
      ':Abstentions': (D(0.0), D(0.0)),
      ':Other exhausted': (D(0.0), D(0.0)),
      ':Total votes': (D(0.0), D(0.0)),
      ':Quota': (D(4.500000001), D(4.500000001)),
      ':Total surplus': (D(0.0), D(0.499999999)),
      ':Iterations': (1, 1)
      }
</code></pre>

<p>where <strong><code>D</code></strong> is an alias for the <strong><code>meeks_prfound.Decimal9.Decimal9</code></strong>
class.</p>

<h3>Caller gives data in JSON files <a id="caller-json"></a></h3>

<p>An alternative way to run a tabulation is with the
<strong><code>meeks_prfound.tabulate_with_json()</code></strong> function which reads the
tabulation input from a named JSON file and writes the results in a JSON
format to a file.</p>

<p>For example, the previous example can be tabulated using a file named
<strong><code>example.json</code></strong> with the following content:</p>

<pre><code>{
  "description": "An example RCV contest"
  ,"nbr_seats_to_fill": 1
  ,"candidates": " A B C"
  ,"ballots": [
    [4, " A B C"],
    [3, " B A C"],
    [2, " C B A"]
    ]
  ,"max_ranking_levels": 3
  ,"tie_breaker": " C A B"
  ,"excluded": ""
  ,"protected": ""
  ,"options": {}
}
</code></pre>

<p>and then executing the Python statement:</p>

<pre><code>elected, status, tally, tab_spec = meeks_prfound.tabulate_with_json(
      'example.json', 'example-results.json')
</code></pre>

<p>which writes a file <strong><code>example-results.json</code></strong> with the following
content:</p>

<pre><code>{
  "description": "An example RCV contest",
  "elected": ["B"],
  "status": [
    ["B", "elected", 2, 5.0, 1.0],
    ["A", "defeated", 2, 4.0, 1.0],
    ["C", "defeated", 1, 2.0, 0.0]
  ],
  "tally": {
    "B": [3.0, 5.0],
    "A": [4.0, 4.0],
    "C": [2.0],
    ":Votes for candidates": [9.0, 9.0],
    ":Overvotes": [0.0, 0.0],
    ":Abstentions": [0.0, 0.0],
    ":Other exhausted": [0.0, 0.0],
    ":Total votes": [9.0, 9.0],
    ":Quota": [4.500000001, 4.500000001],
    ":Total surplus": [0.0, 0.499999999],
    ":Iterations": [1, 1]
  }
}
</code></pre>

<p>If the second argument is omitted or set to the empty string, the JSON
output is printed to <strong><code>stdout</code></strong>.</p>

<p>The first three return values are the same as are returned using the
<strong><code>meeks_prfound.tabulate()</code></strong> function.  The fourth return value is a
dict of tabulation specifications reflecting the net result of what was
found in the input JSON file and any included files.  The input JSON
file can have an 'include' property with a value that is a list of JSON
file names from which other property/values are included, but which can
be overridden by corresponding property/values in the primary input JSON
file.</p>

<p>In the above example, the lines indicating that there are no excluded
candidates and no protected candidates could be omitted, since those are
the default values for JSON files.</p>

<h3>Command line using JSON files <a id="command-line"></a></h3>

<p>A third way to run an RCV tabulation is from the command line.  The
previous example could be run as:</p>

<blockquote>
  <p>python -m meeks_prfound example.json example-results.json</p>
</blockquote>

<p>Depending on the configuration of your computer, other command names
might be used, for example <strong><code>python3</code></strong> instead of <strong><code>python</code></strong> when
running a Python version 3.x.</p>

<p>If the <strong><code>example-results.json</code></strong> argument is omitted or given as an
empty string, the JSON results are printed to <strong><code>stdout</code></strong>.</p>

<h2>Testing <a id="testing"></a></h2>

<p>Tests can be run using the Python unittest module.  For example in a
local copy of this repository, change the working directory to the
<strong><code>tests</code></strong> directory and run one of the following commands:</p>

<blockquote>
  <p>python -m unittest discover</p>

<p>python3 -m unittest discover</p>
</blockquote>

<p>That should run 276 tests, all without errors, typically in less than a
second, though speeds vary depending on the type of computer being used.</p>

<p>There are two kinds of tests in the <strong><code>tests</code></strong> directory tree:</p>

<ul>
<li><p>traditional unittest tests specified in
<strong><code>tests/unittest/test\*.py</code></strong>
files and which are typically dependent on the internal design and
implementation of the meeks_prfound package implementation</p></li>
<li><p>tests with inputs and expected results that are specified in JSON
files and which are designed to be typically applicable to other
conforming implementations of the reference rule.</p></li>
</ul>

<p>The JSON-based tests are typically run from a test*.py file in the same
directory as the JSON files specifying the test.  A group of related
tests are specified in similarly named JSON files, sometimes sharing a
base JSON file for common data.  For example, files <strong><code>abc-007-1.json</code></strong>
and <strong><code>abc-007-2.json</code></strong> might specify two related tests and reference
(include and possibly override) a common <strong><code>abc-007-base.json</code></strong> file.</p>

<p>The JSON files specify a JSON object which is convertible to a
Python dict and which includes a <strong><code>"description"</code></strong> name / key.</p>

<p>Global parameters for the JSON-based test cases may be set in the file
<strong><code>tests/all-tests-spec.json</code></strong>.</p>

<p>The programs have been written and tested for Python versions 2.7.x,
beginning with 2.7.3, and versions 3.x, beginning with 3.2.3.  The unit
tests for the command line interface assume that the Python command name
is <strong><code>python</code></strong> for versions 2.7.x and <strong><code>python3</code></strong> for versions 3.x.
Change the values for <strong><code>PYTHON_2_CMD</code></strong> and <strong><code>PYTHON_3_CMD</code></strong> in
<strong><code>tests/unit/test_json.py</code></strong> if your computer uses different commands.</p>

<h2>Limitations <a id="limitations"></a></h2>

<p>This implementation focuses on the core vote counting algorithms and
does not offer all of the functionality that might be expected to
support tabulation of an election. For example, it does not directly
supply support for end-user report formats.</p>

<p>Neither the programs nor the test cases enforce specific maximum
limitations on the sizes of input to the STV tabulation.  Sizes of
contests that can be tabulated generally depend on the amount of
resources, especially memory, available to the software.</p>

<p>This package can internally handle arbitrarily large numeric values.
However sufficiently large numeric values may lose precision when
converting to or from a JSON format for input or output.  Converting to
or from a JSON format involves conversions to Python's float type. There
should be no loss of precision for vote totals that are less than 9
million.</p>

<p>As a reference implementation written in Python, optimizing the amount
of memory, CPU time, or elapsed time to tabulate votes was generally not
a priority.  Instead, there has been some preference for writing the
programs so that they more closely and simply parallel the provisions of
the reference rule.</p>

<p>Running the automated tests in Python 3 requires at least version 3.2.</p>

<p>In order to focus on issues of core functionality and to allow programs
to run under different versions of Python, all test data has been
limited to using ASCII characters.  Non-ASCII characters are not
supported.</p>

<h2>Extensions <a id="extensions"></a></h2>

<p>This implementation includes several extensions to the reference rule.
These include::</p>

<ul>
<li>anomalous ranking patterns</li>
<li>reporting some exhausted votes as abstentions</li>
</ul>

<p>The anomalous ranking patterns that are allowed include::</p>

<ul>
<li>duplicate rankings (ranking the same candidate more than once at
different ranking levels)</li>
<li>overvoted rankings (ranking two or more different candidates at
the same ranking level on a ballot)</li>
</ul>

<p>The reference rule implicitly assumes that these anomalous ranking
patterns do not occur in the ballots submitted for tabulation.  See the
documentation for the <strong><code>meeks_prfound.meeks.Tabulation</code></strong> class for the
specifics of how these anomalous ranking patterns are handled.</p>

<p>Votes that are not counted for any candidate because all voter-specified
rankings have been exhausted are reported in two categories: abstentions
and other exhausted.  Such votes are counted as abstentions if the voter
did not use all available ranking levels to rank different candidates at
each ranking level, for example by not ranking any candidates at some
ranking levels or by using duplicate rankings.</p>

<p>There is an option, turned on by default, that counts votes for the
first round even if doing so is not required to determine all winners
because there is not an excess of hopeful candidates.  The reference
rule can be strictly followed in this regard by using the option
<code>{'always_count_votes': False}</code> in Python, or <code>{"always_count_votes":
false}</code> in a JSON format.  The value of this option has no effect if
there is an excess of hopeful candidates and as a result, counting votes
for one or more rounds is required to determine winners.</p>

<p>There is an option, <strong><code>"type_of_altdefs"</code></strong>, that controls the point in
the tabulation algorithm at which the opportunity to do multiple
simultaneous defeats is checked.  In order for this option to have an
effect on a round, the <strong><code>"alternative_defeats"</code></strong> option must be a
<strong><code>"Y"</code></strong> for the round.  In addition to doing the check at the end of
step B.2.e, per the reference rule, there are also options to check in
the middle of step B.2.e, if no candidates were elected in the
iteration, and at the beginning of step B.3, just before doing
end-of-round single defeats.</p>

<p>There is support to exclude and protect candidates.  An excluded
candidate is not eligible to be elected or receive votes and is marked
as defeated before the first round of tabulation.  The reference rule
uses the term "withdrawn" rather than "excluded".  A protected candidate
is assured of being elected.</p>

<p>The ability to exclude and protect candidates can be useful when using
the ballots of a previous election to fill one or more vacancies.  For a
typical use case, exclude all previous candidates who are unwilling or
otherwise not eligible to fill the vacancies, protect all candidates
that will continue to serve, set the number of seats to fill to the sum
of the number of protected candidates plus the number of vacancies to
fill, and then retabulate the contest.</p>

<p>When there are protected candidates, there are separate quotas for
protected candidates and unprotected candidates in order to ensure that
an excess of unprotected candidates will not be elected.</p>

<h2>Licensing <a id="licensing"></a></h2>

<p>This project is licensed under the Apache License, Version 2.0 (the
"License"); you may not use contents of this repository except in
compliance with the License.  A copy of the License is in the LICENSE
file and may also be obtained at:</p>

<pre><code>http://www.apache.org/licenses/LICENSE-2.0
</code></pre>

<p>Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.</p>

<p>Copyright 2016-2019 David Cary; licensed under Apache License, Version 2.0</p>
