# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""
A reference implementation for tabulating Meek's method per prfound.org
"""

import sys

from .meek import tabulate
from .status import Status

def version():
  return (3,0,0)

def version_string():
  return "3.0.0"

