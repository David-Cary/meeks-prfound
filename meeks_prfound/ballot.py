# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""Support a ballot during Meek tabulation"""

from __future__ import print_function

from meeks_prfound import errors
import meeks_prfound.constants as K
from meeks_prfound.constants import Decimal

class Ballot(object):
  """A class representing a ballot during Meek tabulation """

  _multiple = 0
  _rankings = tuple()

  def __init__(self, multiple, rankings):
    """
    Initialize an Meek tabulation ballot

    The ballot class represents a group of ballots, all of
    which have the same rankings.

    The requirements for the arguments are the same as for a ballot
    group in the ballots argument for the meek.tabulate() function.  The
    arguments should already have been processed by an appropriate
    validation and formatting routine.

    Arguments
    ---------
    multiple
      The number of ballots in the ballot group.

    rankings
      The candidate rankings for the ballot group.

    """

    self._multiple = multiple
    self._rankings = tuple(rankings)

  def get_multiple(self):
    """Get the number of ballots in this ballot group"""
    return self._multiple

  def get_rankings(self):
    """Get the rankings"""
    return self._rankings

  def __repr__(self):
    """Convert ballot to a string that shows the transfer value"""
    result = '({}, {})'.format(self._multiple, self._rankings)
    return result

  def __str__(self):
    """Convert ballot to a string that does not show the transfer value"""
    result = '({}, {})'.format(self._multiple, self._rankings)
    return result

  def as_tuple(self):
    result = (self._multiple, self._rankings)
    return result

  def __eq__(self, other):
    """
    Is self equal to other?

    Equality is based on self's attributes for _multiple and _rankings.
    The argument other may have correspondingly named attributes or
    indexable keys.  Attributes of other take precedence over indexable
    keys.

    """
    is_equal = True
    try:
      if ((hasattr(other, '_multiple') and
            self._multiple == other._multiple) or
            (hasattr(other, '__getitem__') and
            self._multiple == other['_multiple'])):
        pass
      else:
        return False
      if ((hasattr(other, '_rankings') and
            self._rankings == other._rankings) or
            (hasattr(other, '__getitem__') and
            self._rankings == other['_rankings'])):
        pass
      else:
        return False
    except Exception as exc:
      is_equal = False
    return is_equal

  def __ne__(self, other):
    """negation of __eq__"""
    return not self.__eq__(other)

