# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""Tabulate RCV / STV per prfound.org's Meek's method"""

from __future__ import print_function

from meeks_prfound import constants as K
from meeks_prfound import errors
from meeks_prfound import status
from meeks_prfound import validate

import pdb

# A convenience method to the Tabulation class

def tabulate(nbr_seats_to_fill, candidates, ballots,
      max_ranking_levels, tie_breaker, excluded, protected, options={}):
  """
  Tabulate an RCV / STV contest per prfound.org's Meek's method

  This is a convenience function for accessing the Tabulation class.

  Create an instance of the Tabulation class, using the supplied
  arguments, then call the instance's tabulate() method, returning the
  results from that method.

  Arguments
  ---------
  The same as the __init__ method of the Tabulation class.

  Returns
  -------
  The same as the tabulate method of the Tabulation class.

  Raises
  ------
  The same as the tabulate method of the Tabulation class.

  """
  return Tabulation(nbr_seats_to_fill, candidates, ballots,
      max_ranking_levels, tie_breaker, excluded, protected, options
      ).tabulate()

class Tabulation(object):
  """
  A class for Meek tabulations per prfound.org's reference rule.

  Implements the tabulation logic for Meek RCV / STV as described at:

    https://prfound.org/resources/reference/reference-meek-rule/

  Typical use of this class is to instantiate it and then run its
  tabulate() method to get the results.

  """


  def __init__(self, nbr_seats_to_fill, candidates, ballots,
        max_ranking_levels, tie_breaker, excluded=(), protected=(),
        options={}):
    """
    Initialize a tabulation for a Meek STV contest per prfound.org

    This tabulation class differs from the reference rule for Meek's
    method, as described by prfound.org in the following ways:

    1. This tabulation routine accepts ballots that have anomalous
    ranking patterns, such as duplicate rankings (ranking the same
    candidate at more than one ranking level), skipped ranking levels,
    and overvoted ranking levels (more than one candidate ranked at the
    same ranking level).  The prfound.org algorithm is described in
    terms that implicitly assume that such duplicate rankings and
    overvoted ranking levels do not exist in the ballots it processes.

    For a duplicate ranking, this tabulation will consider only the most
    preferred ranking of a candidate, rankings at less preferred ranking
    levels for the same candidate are ignored.

    Skipped ranking levels, a ranking level without a ranking, is
    ignored, whether or not less preferred ranking levels do have
    rankings.

    An overvoted ranking level causes that ranking level and all less
    preferred ranking levels to be ignored.


    Terminology
    -----------

    Several arguments or parts of an argument, namely candidates,
    rankings in a ballot, tie_breaker, and the value of the
    'alternative_defeats' option, are or may be an ordered
    collection of string values.

    An ordered collection of string values may be represented as one of
    the following:

      - a tuple of strings
      - a list of strings
      - a string that is either the empty string, representing an empty
        tuple, or a delimiter separated list of string values where the
        first character of the string identifies the delimiter.  The
        delimiter character may not be part of any of the component
        string values.

    The following Python expressions specify equivalent ordered
    collections of string values::

      ('A', 'B', '', 'C', '#', 'D')
      ['A', 'B', '', 'C', '#', 'D']
      ' A B  C # D'
      '|A|B||C|#|D'


    Arguments
    ---------
    nbr_seats_to_fill
      The number of seats to fill, an integer that is at least one (also
      referred to as the maximum allowed number of winners or the number
      of candidates to elect).

    candidates
      An ordered collection of strings, each representing a unique
      candidate.  Candidate names may not begin with a colon (':'), may
      not be the empty string, and may not be the string '#'.

    ballots
      A list or tuple of ballot groups.  A ballot group represents a
      number of ballots with the same rankings.  A ballot group is a
      list or tuple of length two with the following values:

        multiple
          A positive integer indicating how many individual ballots are
          summarized into this ballot group.

        rankings
          An ordered collection of strings, each of which is a candidate
          name or other ranking code, ordered from most preferred first
          to least preferred last.

          Other ranking codes are:
            - the empty string, indicating a skipped ranking
            - the string '#', indicating an overvote ranking

          Trailing, skipped (empty) ranking codes do not have to be
          specified.

          Note: This representation of candidate rankings relies on an
          implicit numerical ranking equal to the index of the
          ennumeration of a candidate name and use of the special string
          '#' to indicate an overvote.


    max_ranking_levels
      The maximum number of candidates that each voter is allowed to
      rank on a ballot, i.e. the maximum length of a ballot's rankings,
      when expressed as a tuple. The value for max_ranking_levels must
      be None or an integer that is at least three.  If it is None,
      there is no restriction on the length of a ballot's rankings.

    tie_breaker
      A specification of candidate names.  When two or more
      candidates are tied to be defeated, the earlier candidate is
      chosen to be defeated.

      The tie_breaker can represent a random ordering of the candidates,
      determined by lot.  This is a sufficient tie breaking procedure
      that is compatible with the prfound.org reference rule.

    excluded
      A collection of candidate names that are excluded from the
      tabulation, i.e. are not eligible to be elected or receive votes,
      and are designated as defeated before the first round of
      tabulation begins.

    protected
      A collection of candidate names that are protected from being
      defeated and so are guaranteed of being elected.  A protected
      candidated is not allowed to also be an excluded candidate.
      Designating protected candidates can be useful for example when
      filling a vacancy by retabulating the ballots of a previous
      election.

      When there are protected candidates, there are separate quotas for
      protected candidates and unprotected candidates, in order to
      assure that an excess of unprotected candidates are not elected.

      Protected candidates are elected subject to a protected quota,
      which is equal to what the regular quota would be if no candidates
      were protected.  A protected candidate will be elected at the end
      of a tabulation if the candidate was not already elected by
      reaching the protected quota.

      Unprotected candidates are elected subject to a quota that is
      typically higher than the regular quota, and declines as
      protected candidates increase their non-surplus votes.  If all
      protected candidates are elected by reaching the protected quota,
      the quota for unprotected candidates reduces to approximately
      (allowing for small differences related to rounding and finite
      precision arithmetic) what the regular quota would be if no
      candidates were protected.

      More specifically, the quota for unprotected candidates is based
      on the number seats that are available to unprotected candidates
      and the number of votes that are counting for or could be
      transferred to unprotected candidates, given the current protected
      quota.

      The number of seats available to unprotected candidates is the
      number of seats to be filled by the contest minus the number of
      protected candidates.  The number of seats available to
      unprotected candidates plus one is used in the denominator of the
      quota calculation.

      The number of votes used in the numerator of the quota
      calculation is equal to the number of votes counting for
      unprotected candidates plus the number of surplus votes counting
      for protected candidates (surplus votes in excess of the protected
      quota).  Votes counting for each protected candidate, but only up
      to the protected quota, are not included in the calculation of the
      unprotected quota.


    options
      A dictionary of tabulation options, containing zero or more of the
      following keys and values.
      Default value: a dictionary with each option's default value.

        'always_count_votes'
          A boolean value indicating whether to count votes for the
          first round even if the number of hopeful candidates is less
          than or equal to the number of seats to fill, i.e. all of the
          hopeful candidates can be elected regardless of what the
          first-round vote totals are or whether any votes are counted.
          Allowed values for this option are::

            True - Always count votes for the first round.

            False - Do not count votes for any round if winners can
                  otherwise be determined.

          A value of False specifies tabulation behavior that strictly
          follows the reference rule.  A value of True provides for an
          extension which does not change which candidates are elected
          and produces the same vote totals as the reference rule,
          except for always producing first-round vote totals.
          Default value: True

        'alternative_defeats'
          A value indicating when the option to perform alternative
          defeats should be exercised if they are allowed.  The value
          may be a string or an ordered collection of string values.
          Default value: 'N'

          The rules for alternative defeats specified by the prfound.org
          reference rule are for multiple simultaneous defeats, i.e.
          defeating more than one candidate in a round, but only if
          defeating a single candidate would otherwise be allowed and
          required.

          The value for this option may be one of the following
          case-insensitive string values, indicating when to do
          alternative defeats, if they are allowed:

            'Y' yes, always

            'N' no, never

          The value for this option may also be an ordered collection of
          string values, each string equal to one of the values listed
          above, one for each round of the tabulation.  Extra values in
          the tuple are allowed and are ignored.

          Any sequence of round-by-round choices may be replicated with
          this option.

          The reference rule for when multiple simultaneous defeats are
          allowed is designed with the goal that performing multiple
          simultaneous defeats will not change which candidates win
          compared to defeating at most one candidate per round.
          However since Meek's Method only calculates approximations of
          an exact solution to a system of equations, there may be
          cases where performing multiple simultaneous defeats produces
          a different set of winners.

        'type_of_altdefs'
          A string value indicating when during a round or iteration the
          tabulation should check for multiple simultaneous defeats,
          provided that they are allowed by the 'alternative_defeats'
          option for that round.  Allowed values for this option are::

            'per_reference_rule' - check for multiple simultaneous
                  defeats according to the reference rule, which is at
                  the end of step B.2.e, i.e. iff it is assured that at
                  least one more iteration will be performed for the
                  round.

                  Note that the reference rule will not check for
                  multiple simultaneous defeats on the last iteration of
                  a round, which in some cases can be the only iteration
                  of the round, for example a first round which does not
                  elect any candidates.

            'before_single_defeats' - check for multiple simultaneous
                  defeats at the beginning of step B.3, the step for
                  checking for a single defeat, after all iterations for
                  the round have been completed and there were no
                  candidates elected in the round.

            'if_no_new_electeds' - check for multiple simultaneous
                  defeats in the middle of step B.2.e, after checking
                  that there are no candidates elected in the round, but
                  before checking any conditions for total surplus.

                  This will defeat at least some candidates if
                  'before_single_defeats' would have defeated any
                  candidates. This option can find some candidates to
                  defeat with fewer iterations and greater total
                  surplus, but it will also sometimes defeat a smaller
                  set of candidates.

          All of these types of alternative defeats can result in just a
          single candidate being defeated.  If a single candidate is
          defeated by alternative defeats, it is the same candidate that
          would have been defeated by regular single defeat, i.e.
          without using alternative defeats in that round.

          For any type of alternative defeats, if any candidate is
          defeated as a result of checking for alternative defeats, the
          iteration is stopped at that point and the round is completed
          without further iterations.
          Default value: 'before_single_defeats'


    Raises
    ------
    Same as for Tabulation.tabulate()

    """
    try:
      self.nbr_seats_to_fill = nbr_seats_to_fill
      self.candidates = candidates
      self.ballots = ballots
      self.max_ranking_levels = max_ranking_levels
      self.tie_breaker = tie_breaker
      self.excluded = excluded
      self.protected = protected
      self.options = options
      validator = validate.Validator()
      self.nbr_seats_to_fill = validator.nbr_seats_to_fill(
            self.nbr_seats_to_fill)
      self.candidates = validator.candidates(self.candidates)
      self.max_ranking_levels = validator.max_ranking_levels(
            self.max_ranking_levels)
      self.ballots = validator.ballots(self.ballots,
            self.candidates, self.max_ranking_levels)
      self.tie_breaker = validator.tie_breaker(self.tie_breaker,
            self.candidates)
      self.excluded = validator.excluded(self.excluded, self.candidates)
      self.protected = validator.protected(self.protected, self.candidates,
            self.excluded, self.nbr_seats_to_fill)
      options_validated = validator.options(self.options)
      self.options = {
            K.OPTION_ALTERNATIVE_DEFEATS:
            K.OPTION_ALTERNATIVE_DEFEATS_NEVER,
            K.OPTION_TYPE_OF_ALTDEFS:
            K.OPTION_TYPE_OF_ALTDEFS_BEFORE_SINGLE_DEFEATS,
            K.OPTION_ALWAYS_COUNT_VOTES: True
            }
      self.options.update(options_validated)
    except (errors.MeekValueError, errors.MeekImplementationError):
      raise
    except (MemoryError, SystemError):
      raise
    except Exception as exc:
      raise errors.MeekImplementationError(
            'Possible Meek implementation error:', (), exc)

  def tabulate(self, **kwargs):
    """
    Perform a tabulation of an RCV / STV Meek contest

    Arguments
    ---------
    Normally this method is called with no arguments.  However to
    facilitate unit testing, the following arguments may be provided as
    keyword arguments to artificially stop the tabulation at various
    points in a given round.  Values of None mean the tabulation is not
    artificially stopped.

    stop_at_begin
      The number of a round; stop at the beginning of that round.
      Default value: None

    stop_after_status_update
      The number of a round; stop after the vote tally and status tally
      update for that round.
      Default value: None

    stop_at_end
      The number of a round; stop at the end of that round.
      Default value: None

    Returns
    -------
    A tuple with the following values in order:

      elected
        A set of candidate names that have been elected by the
        tabulation

      status
        A dictionary keyed by candidate name for every candidate, each
        with a value that is a status.Status object, which has the
        following attributes:

          candidate
            The candidate's name.

          status
            A string indicating the candidate's status: 'elected' or
            'defeated'.

          nbr_round
            The 1-based number of the round for which votes were
            calculated when the candidate was elected or defeated; zero
            if the candidate was excluded.

          votes
            The number of votes the candidate had when elected or
            defeated; None if the candidate was excluded.

          keep_factor
            The fraction of a ballot's vote weight that counted for
            the candidate when votes were last tallied/distributed.

          destiny
            An indication of whether the candidate was excluded or
            protected or neither, i.e. normal.  This attribute may be
            omitted in some formats, including JSON formats, if the
            destiny is normal.

      tally
        A dictionary, keyed by candidate names and labels for other
        tabulation categories, of tuples of round-by-round vote totals
        or other statistics.

        A vote total for the kth round is accessed with an index of k-1.
        A candidate has a vote total only for those rounds which started
        with the candidate as a hopeful or elected candidate.

        In addition to candidates, there are vote totals or counts for
        all rounds for each of the following keys / tabulation
        categories::

          ':Votes for candidates'
          ':Overvotes'
          ':Abstentions'
          ':Other exhausted'
          ':Total votes'
          ':Protected quota
          ':Quota votes
          ':Quota'
          ':Total surplus'
          ':Iterations'

        ':Protected quota' and ':Quota votes' are present iff there
        are some protected candidates.

        ':Quota votes' identifies the number of votes used to calculate
        the ':Quota' for unprotected candidates and is equal to the
        number of votes counting for unprotected candidates, plus any
        surplus votes for protected candidates (based on the protected
        quota).

        The list for the ':Iterations' key contains integer values,
        indicating the number of iterations that were performed for the
        round.

    Raises
    ------
    MeekValueError
      If any values passed to this function do not pass validation
      checks.

    MeekImplementationError
      If an inconsistency is detected during the tabulation that is not
      attributable to invalid arguments, resource constraints, resource
      availability, or other external interventions.  If this exception
      is raised, it might be because this Python package contains a
      logic error.

    Other exceptions:
      Other exceptions defined by Python and its standard libraries can
      be raised, for example as a result of unavailable or insufficient
      resources.

      This function does not otherwise impose restrictions on the size
      of the tabulation, including, the number of seats to be elected,
      the number of candidates, the number of ballots or ballot groups,
      the number of rankings per ballot, or the length of candidate
      names.  The size of a tabulation that can be performed is
      primarily dependent on the resources of the hardware and software
      configuration on which the tabulation runs.

    The MeekValueError and MeekImplementationError classes are
    indirectly subclasses of the Exception class and neither is a
    subclass of the other, directly or indirectly.

    """
    try:
      self.testing = {'stop_at_begin': None, 'stop_after_status_update': None,
            'stop_at_end': None}
      self.testing.update(kwargs)
      elected, status, tally = self._tabulate_meeks()
    except (errors.MeekValueError, errors.MeekImplementationError):
      raise
    except (MemoryError, SystemError):
      raise
    except Exception as exc:
      raise errors.MeekImplementationError(
            'Possible Meek STV implementation error:', (), exc)
    return elected, status, tally

  def _other_categories(self):
    result = {other_label: [] for other_label in K.OTHER_LABELS_LIST}
    if len(self.protected) == 0:
      del result[K.LABEL_PROTECTED_QUOTA]
      del result[K.LABEL_QUOTA_VOTES]
    return result

  def _tabulate_meeks(self):
    """
    Tabulate a Meek's STV contest.

    This method is for internal use only.

    """
    self._tabulate_setup()
    while self._process_a_meeks_round():
      if self.testing['stop_at_end'] == self.nbr_round:
        break
    return self._elected(), self.status, self.tallies

  def _tabulate_setup(self):
    """
    Create instance values needed for tabulation
    """
    self.nbr_round = 0
    self.tallies = {candidate: [] for candidate in self.candidates}
    self.tallies.update(self._other_categories())
    self.nbr_unprotected_seats = self.nbr_seats_to_fill - len(self.protected)
    # Reference rule step A, Initialize Election
    self.status = {candidate: status.Status(candidate, None,
          nbr_round=0, destiny=self._get_candidate_destiny(candidate))
          for candidate in self.candidates}
    for cstatus in self.status.values():
      if cstatus.candidate in self.excluded:
        cstatus.status = K.STATUS_DEFEATED
    self.omega = K.ULP * 1000
    self.keep_factors = {candidate: K.ONE for candidate in self.status}

  def _process_a_meeks_round(self):
    """
    Process vote counting for a Meek round

    Return a boolean indicating whether to continue with another round.

    """
    if (self.nbr_round > 0 or (self.nbr_round == 0 and
          self.options[K.OPTION_ALWAYS_COUNT_VOTES] is False)):
      # Reference rule step B.1, Test count complete
      nbr_elected_or_protected = len(self._elected() | self.protected)
      if (nbr_elected_or_protected == self.nbr_seats_to_fill or
            len(self._hopeful()) + len(self._elected())
            <= self.nbr_seats_to_fill):
        # Reference rule step C.1, Elect remaining
        if nbr_elected_or_protected < self.nbr_seats_to_fill:
          self._elect_candidates(self._hopeful())
        # Reference rule step C.2, Defeat remaining
        else:
          self._elect_candidates(self.protected - self._elected())
          self._defeat_candidates(self._hopeful())
        return False

    self.nbr_round += 1

    if self.testing['stop_at_begin'] == self.nbr_round:
      return False
    self.prev_total_surplus = None
    self.nbr_iteration = 0
    self.defeat_this_round = set()
    # Reference rule step B.2, Iterate
    while self._process_a_meeks_iteration():
      self.prev_total_surplus = self.total_surplus
    for label, votes in self.iter_tally.items():
      if (label not in self.status or
            self.status[label].status != K.STATUS_DEFEATED):
        self.tallies[label].append(votes)
    if self.testing['stop_after_status_update'] == self.nbr_round:
      return False
    if not self.new_electeds:
      # Check for alternative / multiple / batch / simultaneous defeats
      # before single defeats
      if (self._get_alt_defeats_option() == K.OPTION_ALTERNATIVE_DEFEATS_YES and
            self.options[K.OPTION_TYPE_OF_ALTDEFS] ==
            K.OPTION_TYPE_OF_ALTDEFS_BEFORE_SINGLE_DEFEATS):
        self.defeat_this_round = self._get_stv_alternative_defeats()
      # Reference rule step B.3, Defeat low candidate
      if not self.defeat_this_round and self._hopeful():
        self.defeat_this_round = self._get_single_defeat_candidate()
      self._defeat_candidates(self.defeat_this_round)
    if self.testing['stop_at_end'] == self.nbr_round:
      return False
    return True


  def _process_a_meeks_iteration(self):
    self.nbr_iteration += 1
    self._init_iter_tally()
    # Reference rule step B.2.a, Distribute votes
    self._distribute_votes()
    self.total_candidate_votes = self._get_total_candidate_votes()
    self.iter_tally[K.LABEL_TOTAL_CANDIDATE_VOTES] = (
          self.total_candidate_votes)
    self.iter_tally[K.LABEL_TOTAL_VOTES] = (
          self.iter_tally[K.LABEL_TOTAL_CANDIDATE_VOTES] +
          self.iter_tally[K.LABEL_OVERVOTES] +
          self.iter_tally[K.LABEL_ABSTENTIONS] +
          self.iter_tally[K.LABEL_OTHER_EXHAUSTED])
    # Reference rule step B.2.b, Update quota
    self._set_quota()
    self._update_candidate_status_tally()

    if self.testing['stop_after_status_update'] == self.nbr_round:
      return False

    # Reference rule step B.2.c, Find winners
    # candidates with more than a quota of votes are elected
    self.new_electeds = self._get_new_electeds()
    self._elect_candidates(self.new_electeds)
    # Reference rule step B.2.d, Calculate the total surplus
    self.total_surplus = self._get_total_surplus()
    self.iter_tally[K.LABEL_TOTAL_SURPLUS] = self.total_surplus
    # Reference rule step B.2.e, Test for iteration finished
    # Test whether this is the final iteration for the round
    if len(self.new_electeds):
      return False
    # Check for alternative / multiple / batch / simultaneous defeats
    # if no new electeds
    if (self._get_alt_defeats_option() == K.OPTION_ALTERNATIVE_DEFEATS_YES and
          self.options[K.OPTION_TYPE_OF_ALTDEFS] ==
          K.OPTION_TYPE_OF_ALTDEFS_IF_NO_NEW_ELECTEDS):
      self.defeat_this_round = self._get_stv_alternative_defeats()
      if self.defeat_this_round:
        return False
    if (self.total_surplus < self.omega):
      return False
    if (self.prev_total_surplus is not None and
          self.total_surplus >= self.prev_total_surplus):
      return False
    # Check for alternative / multiple / batch / simultaneous defeats
    # per reference rule, possibly with deferred surplus distribution
    if (self._get_alt_defeats_option() == K.OPTION_ALTERNATIVE_DEFEATS_YES and
          self.options[K.OPTION_TYPE_OF_ALTDEFS] ==
          K.OPTION_TYPE_OF_ALTDEFS_PER_REF_RULE):
      self.defeat_this_round = self._get_stv_alternative_defeats()
      if self.defeat_this_round:
        return False
    # Reference rule step B.2.f, Update keep factors
    self._update_keep_factors()
    return True

  def _init_iter_tally(self):
    if self.nbr_round == 1:
      self.iter_tally = {tab_code: K.ZERO for tab_code in self.tallies}
    else:
      self.iter_tally = {tab_code:
            None if tab_code in self.status and
              self.status[tab_code].status == K.STATUS_DEFEATED
              else K.ZERO
            for tab_code in self.tallies}
    self.iter_tally[K.LABEL_NBR_ITERATIONS] = self.nbr_iteration

  def _distribute_votes(self):
    """
    Distribute votes from ballots to candidates
    """
    for ballot in self.ballots:
      ballot_weight = K.ONE
      is_ranked = set()
      multiple = ballot.get_multiple()
      for ranking_index, ranking_code in enumerate(ballot.get_rankings()):
        if ranking_code == K.RANKING_CODE_OVERVOTE:
          self.iter_tally[K.LABEL_OVERVOTES] += ballot_weight * multiple
          ballot_weight = K.ZERO
          break
        if ranking_code in self.status:
          if (self.status[ranking_code].status in
              (K.STATUS_HOPEFUL, K.STATUS_ELECTED) and
              ranking_code not in is_ranked ):
            ranking_keep = ballot_weight.multiply_by(
                  self.keep_factors[ranking_code], True)
            self.iter_tally[ranking_code] += ranking_keep * multiple
            ballot_weight -= ranking_keep
            if ballot_weight == K.ZERO:
              break
          is_ranked.add(ranking_code)

      if ballot_weight > K.ZERO:
        exhausted_votes = ballot_weight * multiple
        if (self.max_ranking_levels is None or
              len(is_ranked) < self.max_ranking_levels):
          self.iter_tally[K.LABEL_ABSTENTIONS] += exhausted_votes
        else:
          self.iter_tally[K.LABEL_OTHER_EXHAUSTED] += exhausted_votes

  def _get_candidate_destiny(self, candidate):
    result = K.DESTINY_NORMAL
    if candidate in self.excluded:
      result = K.DESTINY_EXCLUDED
    elif candidate in self.protected:
      result = K.DESTINY_PROTECTED
    return result

  def _get_total_candidate_votes(self):
    result = sum([self.iter_tally[candidate]
          for candidate in self.status
          if self.iter_tally[candidate] is not None], K.ZERO)
    return result

  def _set_quota(self):
    quota = self.total_candidate_votes / (self.nbr_seats_to_fill + 1) 
    quota += K.ULP
    self.quota = quota
    if len(self.protected):
      self.protected_quota = quota
      self.iter_tally[K.LABEL_PROTECTED_QUOTA] = self.protected_quota
      self.protected_votes = sum([
            min(self.iter_tally[candidate], self.protected_quota)
            for candidate in self.protected], K.ZERO)
      self.quota_votes = self.total_candidate_votes - self.protected_votes
      self.iter_tally[K.LABEL_QUOTA_VOTES] = self.quota_votes
      quota = self.quota_votes / (self.nbr_unprotected_seats + 1)
      self.quota = quota + K.ULP
    self.iter_tally[K.LABEL_QUOTA] = self.quota

  def _get_quota(self, candidate):
    result = (self.protected_quota if candidate in self.protected
          else self.quota)
    return result

  def _update_candidate_status_tally(self):
    index_round = self.nbr_round - 1
    for candidate, candidate_status in self.status.items():
      if candidate_status.status == K.STATUS_HOPEFUL:
        candidate_status.nbr_round = self.nbr_round
        candidate_status.votes = self.iter_tally[candidate]
      candidate_status.keep_factor = self.keep_factors[candidate]

  def _get_total_surplus(self):
    result = sum([self.iter_tally[candidate] - self._get_quota(candidate)
          for candidate, status in self.status.items()
          if status.status == K.STATUS_ELECTED], K.ZERO)
    return result

  def _update_keep_factors(self):
    for candidate, keep_factor in self.keep_factors.items():
      if self.status[candidate].status == K.STATUS_ELECTED:
        quota = self._get_quota(candidate)
        product = keep_factor.multiply_by(quota, True)
        new_keep_factor = product.divide_by(self.iter_tally[candidate], True)
        self.keep_factors[candidate] = new_keep_factor


  def _elected(self):
    """
    Provide a set of the elected candidates
    """
    index_round = self.nbr_round - 1
    elected_candidates = set([candidate
          for candidate in self.status
          if self.status[candidate].status == K.STATUS_ELECTED])
    return elected_candidates

  def _hopeful(self):
    """
    Provide a set of the hopeful candidates
    """
    index_round = self.nbr_round - 1
    hopeful_candidates = set([candidate
          for candidate in self.status
          if self.status[candidate].status == K.STATUS_HOPEFUL])
    return hopeful_candidates

  def _hopeful_votes(self):
    """
    Get a dict of hopeful candidates and their vote totals
    """
    hopeful_votes = {candidate: self.status[candidate].votes
          for candidate in self._hopeful()}
    return hopeful_votes

  def _get_new_electeds(self):
    """
    Get a dict of hopeful candidates that have reached the quota
    """
    new_electeds = {
          candidate: self.iter_tally[candidate]
          for candidate, status in self.status.items()
          if status.status == K.STATUS_HOPEFUL and
                self.iter_tally[candidate] >= self._get_quota(candidate)}
    return new_electeds

  def _elect_candidates(self, candidates):
    """
    Update the status of each candidate in the list
    """
    for candidate in candidates:
      if self.status[candidate].status == K.STATUS_HOPEFUL:
        self.status[candidate].status = K.STATUS_ELECTED
      else:
        raise errors.MeekImplementationError(
              'Attempting to elect a candidate that is not hopeful.', [
              ('candidate', candidate),
              ('status', self.status[candidate].status),
              ('round', self.nbr_round)
              ])

  def _defeatable(self):
    """
    Provide a set of the hopeful candidates that are not protected
    """
    result = set([candidate
          for candidate in self.status
          if self.status[candidate].status == K.STATUS_HOPEFUL and
          candidate not in self.protected])
    return result

  def _defeatable_votes(self):
    """
    Get a dict of defeatable candidates and their vote totals
    """
    defeatable_votes = {candidate: self.status[candidate].votes
          for candidate in self._defeatable()}
    return defeatable_votes

  def _defeat_candidates(self, candidates):
    """
    Defeat the collection of candidates
    """
    for candidate in candidates:
      if (self.status[candidate].status == K.STATUS_HOPEFUL and
            candidate not in self.protected):
        self.status[candidate].status = K.STATUS_DEFEATED
        self.keep_factors[candidate] = K.ZERO
      else:
        raise errors.MeekImplementationError(
              'Attempting to defeat a candidate that is not defeatable.', [
              ('candidate', candidate),
              ('status', self.status[candidate].status),
              ('round', self.nbr_round)
              ])

  def _get_single_defeat_candidate(self):
    """
    Get the candidate with the fewest votes, after resolving any tie

    Returns
    -------
    A singleton set of the unprotected, hopeful candidate with the
    fewest votes, after resolving any tie.

    Raises
    ------
    MeekValueError
      If there is a tied candidate not in self.tie_breaker.

    """
    defeatable_votes = self._defeatable_votes()
    min_votes = min(defeatable_votes.values())
    trailing_candidates = set([candidate
          for candidate, votes in defeatable_votes.items()
          if votes <= min_votes + self.total_surplus])
    defeat_candidate = self._resolve_tie(trailing_candidates)
    return set([defeat_candidate])

  def _resolve_tie(self, tied_candidates):
    """
    Select the tied candidate that is earliest in the tie_breaker.

    Arguments
    ---------
    tied_candidates
      A set of one or more candidates that are tied.

    Returns
    -------
    The candidate name with the lowest tie_breaker index.

    Raises
    ------
    MeekValueError
      If there is a tied candidate not in self.tie_breaker.

    """
    if len(tied_candidates) <= 1:
      return list(tied_candidates)[0]
    not_in_tie_breaker = tied_candidates.difference(set(self.tie_breaker))
    if not_in_tie_breaker:
      raise errors.MeekValueError('Tied candidate not in tie_breaker:', (
            ('candidate', not_in_tie_breaker.pop()),
            ('round', self.nbr_round),
            ('tied_candidates', tied_candidates),
            ('tie_breaker', self.tie_breaker),
            ))
    tied_candidates_by_index = {index: candidate
        for candidate, index in self.tie_breaker.items()
        if candidate in tied_candidates}
    selected_candidate = tied_candidates_by_index[
          min(tied_candidates_by_index)]
    return selected_candidate

  def _get_alt_defeats_option(self):
    """
    Get the value for alternative defeats for the current round

    Returns
    -------
    A string that is the option for alternative defeats in the round

    Raises
    ------
    MeekValueError
      If the option is stored as a tuple of strings, one per round, but
      the tuple is too short.

    """
    options_value = self.options[K.OPTION_ALTERNATIVE_DEFEATS]
    if type(options_value) == str:
      alt_defeats_option = options_value
    else:
      try:
        alt_defeats_option = options_value[self.nbr_round - 1]
      except IndexError:
        raise errors.MeekValueError(
              'Alternative defeats option tuple too short.', (
              ('nbr_round', self.nbr_round),
              ('len(options_value)', len(options_value)),
              ))
    return alt_defeats_option

  def _get_stv_alternative_defeats(self):
    """
    Get largest set of STV candidates that can be alternatively defeated

    Returns
    -------
    A set of candidates that can be alternatively defeated per the
    conditions of the reference rule, but allowing for protected,
    hopeful candidates.  In particular, only defeatable candidates, i.e.
    unprotected hopeful candidates, are treated as eligible for defeat,
    either now or at a later point in the tabulation.

    """
    total_surplus_votes = self.total_surplus
    defeatable_votes = self._defeatable_votes()
    total_votes_to_defeat = sum(defeatable_votes.values(), K.ZERO)
    by_votes = sorted(defeatable_votes.items(),
          key=lambda item: item[1], reverse=True)
    candidates_to_defeat = set(defeatable_votes)
    nbr_elected = len(self._elected())
    # Breaking out of this loop produces a set of candidates to defeat,
    #     possibly an empty set.
    # Not breaking out of this loop will produce an empty set.
    nbr_defeated = len(by_votes)
    for ix, (candidate, lowest_remaining_votes) in enumerate(by_votes):
      total_votes_to_defeat -= lowest_remaining_votes
      candidates_to_defeat.remove(candidate)
      nbr_remaining = ix + 1
      nbr_defeated -= 1
      most_votes_to_defeat = (
            by_votes[ix+1][1] if nbr_remaining < len(by_votes) else K.ZERO)
      # Reference rule terminology:
      #   candidate c is by_votes[ix + 1][0] if ix + 1 is a valid index
      #   votes v is most_votes_to_defeat
      #   votes v'' is lowest_remaining_votes
      cond_2 = (total_votes_to_defeat + total_surplus_votes <
              lowest_remaining_votes)
      cond_1 = (nbr_remaining + nbr_elected >= self.nbr_seats_to_fill)
      cond_alt_def = cond_1 and cond_2
      # Test top-level conditions
      if cond_alt_def:
        break
    return candidates_to_defeat


def get_tally_sort_key(code, status):
  """
  Get a tally sort key

  The sort key can be used to sort candidates and other tabulation
  categories, for example the status and tally collections returned by
  meek.Tabulation().tabulate().

  The sort codes will sort candidates before other tabulation
  categories; elected candidates before hopeful candidates before
  defeated candidates; elected candidates by increasing round of
  election, then by decreasing votes; other candidates within their
  status by decreasing round of election, then by decreasing votes; any
  remaining ties are broken by the sort order of candidate names.  Other
  tabulation categories are sorted in the order recorded in
  K.OTHER_LABELS_ORDER.

  Arguments
  =========
  code
    A string representing a candidate name or label of another
    tabulation category.

  status
    A dictionary of tabulation result statuses, as given by the second
    item of the return value from meek.Tabulation().tabulate() or as
    used within that function.

  Returns
  =======
  A sort key in the form of a tuple of integers and/or strings.

  """
  sort_key = tuple([9, code])
  if code in status:
    nbr_round = status[code].nbr_round
    votes = status[code].votes
    if votes is None:
      votes = -K.ONE
    if status[code].status == K.STATUS_ELECTED:
      sort_key = (1, 1, nbr_round, -votes, code)
    elif status[code].status == K.STATUS_HOPEFUL:
      sort_key = (1, 2, -nbr_round, -votes, code)
    else:
      sort_key = (1, 3, -nbr_round, -votes, code)
  else:
    if code in K.OTHER_LABELS_ORDER:
      sort_key = (2, K.OTHER_LABELS_ORDER[code], code)
    else:
      sort_key = (3, 1, code)
  return sort_key

def print_status(status, label1='', label2='"status": ', indent1=2, indent2=4,
      suffix=''):
  """
  Print a dictionary of candidate statuses

  Arguments
  =========
  status
    A dictionary of Status objects, keyed by candidate names.

  label1
    A string that prefixes labeling of the printing of the tally.

  label2
    A string that labels the printing of the tally.
    Default: '"status": '

  indent1
    The number of spaces that prefix the first and last printed lines.

  indent2
    The number of spaces that prefix each printed line other than the
    first and last lines.

  suffix
    A string that is printed at the end of the last line.
    Default: ''

  Returns
  =======
  None

  """
  sorted_status = sorted(status.items(), key=lambda item:
        get_tally_sort_key(item[0], status))
  print('%s%s%s[' % (' ' * indent1, label1, label2))
  print(",\n".join(['%s%s'  % (' ' * indent2,
        str(list(cstatus.as_tuple())))
        for code, cstatus in sorted_status]) +
        '\n%s]%s' % (' ' * indent1, suffix))

def print_tally(tally, status, label1='', label2='"tally": ', indent1=2,
      indent2=4, suffix=''):
  """
  Print a round-by-round tally of votes

  Arguments
  =========
  tally
    A tally object, a dict of sequences, each of votes or counts, keyed
    by candidate names and other label of tabulation categories.

  status
    A dictionary of Status objects, keyed by candidate names.

  label1
    A string that prefixes labeling of the printing of the tally.

  label2
    A string that labels the printing of the tally.
    Default: '"tally": '

  indent1
    The number of spaces that prefix the first and last printed lines.

  indent2
    The number of spaces that prefix each printed line other than the
    first and last lines.

  suffix
    A string that is printed at the end of the last line.
    Default: ''

  Returns
  =======
  None

  """
  sorted_tally = sorted(tally.items(), key=lambda item:
        get_tally_sort_key(item[0], status))
  print('%s%s%s{' % (' ' * indent1, label1, label2))
  print(",\n".join(['%s"%s": %s'  % (' ' * indent2, code,
        repr([float(repr(votes)) if type(votes) == K.Decimal else votes
          for votes in votes_seq]))
        for code, votes_seq in sorted_tally]) +
        '\n%s}%s' % (' ' * indent1, suffix))

