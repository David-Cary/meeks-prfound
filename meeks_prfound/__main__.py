# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""command line interface"""

from __future__ import print_function

from .with_json import tabulate

import sys


# A convenience method for using the meek.Tabulation class from the
# command line using JSON files for input and results.

def tabulate_with_json(input_json='', output_json='', default_json=None):
  tabulate(input_json, output_json, default_json)

#if __name__ == '__main__':
if len(sys.argv) > 2:
  tabulate(sys.argv[1], sys.argv[2])
elif len(sys.argv) > 1:
  tabulate(sys.argv[1], '')


