# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""Constants for an Meek tabulation"""

from meeks_prfound.decimal9 import Decimal9 as Decimal
from meeks_prfound.decimal9 import Decimal9Total as DecimalTotal
from meeks_prfound.decimal9 import ulp

ZERO = Decimal(0)
ONE = Decimal(1)
ULP = ulp()

MIN_RANKINGS_SUPPORTED = 3

RANKING_CODE_SKIPPED = ''
RANKING_CODE_OVERVOTE = '#'
RANKING_CODES_NOT_A_CANDIDATE = set((
      RANKING_CODE_SKIPPED,
      RANKING_CODE_OVERVOTE))

LABEL_TOTAL_CANDIDATE_VOTES = ':Votes for candidates'
LABEL_OVERVOTES = ':Overvotes'
LABEL_ABSTENTIONS = ':Abstentions'
LABEL_OTHER_EXHAUSTED = ':Other exhausted'
LABEL_TOTAL_VOTES = ':Total votes'
LABEL_PROTECTED_QUOTA = ':Protected quota'
LABEL_QUOTA_VOTES = ':Quota votes'
LABEL_QUOTA = ':Quota'
LABEL_TOTAL_SURPLUS = ':Total surplus'
LABEL_NBR_ITERATIONS = ':Iterations'

OTHER_LABELS_LIST = [
      LABEL_TOTAL_CANDIDATE_VOTES, LABEL_OVERVOTES, LABEL_ABSTENTIONS,
      LABEL_OTHER_EXHAUSTED, LABEL_TOTAL_VOTES,
      LABEL_PROTECTED_QUOTA, LABEL_QUOTA_VOTES, LABEL_QUOTA,
      LABEL_TOTAL_SURPLUS, LABEL_NBR_ITERATIONS]
OTHER_LABELS_ORDER = {label: ix for ix, label in enumerate(OTHER_LABELS_LIST)}

OPTION_ALTERNATIVE_DEFEATS = 'alternative_defeats'
OPTION_TYPE_OF_ALTDEFS = 'type_of_altdefs'
OPTION_ALWAYS_COUNT_VOTES = 'always_count_votes'
OPTION_KEY_SET = set([OPTION_ALTERNATIVE_DEFEATS, OPTION_TYPE_OF_ALTDEFS,
      OPTION_ALWAYS_COUNT_VOTES])
OPTION_ALTERNATIVE_DEFEATS_YES = 'Y'
OPTION_ALTERNATIVE_DEFEATS_NEVER = 'N'
OPTION_ALTERNATIVE_DEFEATS_VALUE_SET = set([
      OPTION_ALTERNATIVE_DEFEATS_YES,
      OPTION_ALTERNATIVE_DEFEATS_NEVER,
      ])
OPTION_TYPE_OF_ALTDEFS_PER_REF_RULE = 'per_reference_rule'
OPTION_TYPE_OF_ALTDEFS_BEFORE_SINGLE_DEFEATS = 'before_single_defeats'
OPTION_TYPE_OF_ALTDEFS_IF_NO_NEW_ELECTEDS = 'if_no_new_electeds'
OPTION_TYPE_OF_ALTDEFS_VALUE_SET = set([
      OPTION_TYPE_OF_ALTDEFS_PER_REF_RULE,
      OPTION_TYPE_OF_ALTDEFS_BEFORE_SINGLE_DEFEATS,
      OPTION_TYPE_OF_ALTDEFS_IF_NO_NEW_ELECTEDS
      ])

STATUS_HOPEFUL = 'hopeful'
STATUS_DEFEATED = 'defeated'
STATUS_ELECTED = 'elected'

DESTINY_NORMAL = 'normal'
DESTINY_EXCLUDED = 'excluded'
DESTINY_PROTECTED = 'protected'

