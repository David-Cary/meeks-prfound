""" Filter lines from a text file

usage:
  python* filter.py <selector> <input-file-name> <output-file-name>

"""
from __future__ import print_function

import sys

def filter(selector, in_filename, ot_filename):
  """
  Filter lines from a text file

  Filter, modify, and copy lines of an input file to an output file
  based on a selector character.

    * Lines that do not begin with a '?' are copied unchanged.

    * Lines that begin with '?x ', where x is the selector character or
      '?', are copied without the '?x ' prefix.

    * Lines that begin with '?y ', where y is neither the selector
    character nor '?', are dropped from the output.

  Arguments
  =========
  selector
    A character which identifies lines to select.

  in_filename
    The filename of the input file.

  ot_filename
    The file name of the output file.

  """
  selector = selector + ' '
  with open(in_filename, "r") as infile:
    with open(ot_filename, "w") as otfile:
      for line in infile.readlines():
        if "?" == line[0]:
          if selector == line[1:3] or '? ' == line[1:3]:
            line = line[3:]
          else:
            continue
        otfile.write(line)

if __name__ == "__main__":
  if len(sys.argv) > 3:
    selector = sys.argv[1][:2]
    filter(selector, sys.argv[2], sys.argv[3])
