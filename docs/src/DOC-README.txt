Markdown does not have a consistent way to implement internal
links, how to mark and reference the fragment part of a URL used for an
internal link destination.  As a result, the simple Python filter.py program is
used to generate several flavors of a markdown file.

The README.template.md file contains several variations for implementing
internal links.  Each variation can be selected for use by using a text editor
to  manually delete lines that only apply to other variations editor.  However
the filter.py program helps to automate that task:

  - A version that is compatible with the rendering performed by
    bitbucket.com, which uses fragments corresponding to headers but prefixed
    with 'markdown-header-'.  This version is generated to the top-level
    directory with the command:

    $ python filter.py 1 README-template.md ../../README.md

  - A version that is compatible with several online rendering programs, which
    use fragments corresponding to headers which are lowercased and hyphenated.
    This version is generated with the command:

    $ python filter.py 2 README-template.md ../README-alt.md

  - A version that is compatible with a markdown program that supports use of
    HTML anchor tags mark fragment destinations.  This version is generated,
    and on an Ubuntu system the HTML file is generated to the top-level
    directory, with the commands:

    $ python filter.py 3 README-template.md _readme-html.md
    $ markdown _readme-html.md > ../../README.html

The command python3 may need to be used instead of python.  These commands are
collected in the Bash script, readme-gen. That script can be run with the
command:

    $ bash readme-gen

In README-template.md, the lines that are dependent on the markdown flavor are
prefixed respectively with '?1 ', '?2 ', or '?3 '.  The program filter.py will
select the requested version of those lines and strip the prefix.  It will also
copy unchanged any lines that do not start with a '?'.

In order to support output lines that might begin with a '?' and are not
dependent on the markdown flavor, filter.py will also always select any input
lines that begin with '?? ' and strip that prefix.  However that capability is
not currently used.

