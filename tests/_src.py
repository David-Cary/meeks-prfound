# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""Support test modules to import the meeks_prfound package"""

import os
import sys
sys.path.insert(0, os.path.abspath('..'))

import meeks_prfound
