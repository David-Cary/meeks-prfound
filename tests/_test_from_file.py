# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""Run test cases based on JSON files """

from __future__ import print_function

import _test_aids

from _src import meeks_prfound
from meeks_prfound import with_json
from meeks_prfound import meek
from meeks_prfound import validate
from meeks_prfound import constants as K

import json
import os.path

u2s = with_json.u2s

def run_test_spec(test_case, input_json):
  """
  Run a test case using test specs from a JSON text file
  """
  tabulate_args, test_spec = with_json.build_tabulate_args(
        input_json, 'all-tests-spec.json')
  if 'maxDiff' in test_spec:
    maxDiff = test_spec['maxDiff']
    if maxDiff is None:
      test_case.maxDiff = None
    if type(maxDiff) == int and maxDiff >= 0:
      test_case.maxDiff = maxDiff

  try: print_description = test_spec['print_description']
  except KeyError: print_description = False
  if print_description:
    print('\n  """' + test_spec['description'] + '"""')

  if 'exception' in test_spec:
    exception_type, exception_message = test_spec['exception']
    _test_aids.assertRaises_with_message(test_case,
          u2s(exception_type),
          u2s(exception_message),
          meek.tabulate, tabulate_args)
  else:
    expected_elected = validate.str_tuple(test_spec['elected'])
    expected_status = _test_aids.build_expected_status(
          test_spec['status_codes'])
    expected_tally = {_test_aids.u2s(candidate):
          [vote_total
          for vote_total in votes]
          if candidate == K.LABEL_NBR_ITERATIONS else
          [K.Decimal(vote_total)
          for vote_total in votes]
          for candidate, votes in test_spec['tally'].items()}
    elected, status, tally = meek.Tabulation(*tabulate_args).tabulate()
    if 'print_results' in test_spec and test_spec['print_results']:
      print_elected(elected)
      meek.print_status(status)
      meek.print_tally(tally, status)
      try: description = test_spec['description']
      except KeyError: description = None
      jason_str = with_json.results_to_json(elected, status, tally, stats,
            description)
      print(jason_str)
    status_dict = {candidate: status.as_dict()
          for candidate, status in status.items()}
    test_case.assertEqual(tally, expected_tally)
    test_case.assertEqual(status_dict, expected_status)
    test_case.assertEqual(set(elected), set(expected_elected))


# The following are convenience debugging methods

def print_elected(elected):
  """
  Print a list of elected candidates; for debugging
  """
  print("elected:", sorted(elected))

