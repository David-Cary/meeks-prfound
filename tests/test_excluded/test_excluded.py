# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_from_file


class TestExcluded(unittest.TestCase):
  """Test Meeks excluded candidates from file-based specs"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = 800

  def test_excl_001_1(self):
    _test_from_file.run_test_spec(self, 'test_excluded/excl-001-1.json')

  def test_excl_001_2(self):
    _test_from_file.run_test_spec(self, 'test_excluded/excl-001-2.json')

  def test_excl_001_3(self):
    _test_from_file.run_test_spec(self, 'test_excluded/excl-001-3.json')

  def test_excl_001_4(self):
    _test_from_file.run_test_spec(self, 'test_excluded/excl-001-4.json')

  def test_excl_001_5(self):
    _test_from_file.run_test_spec(self, 'test_excluded/excl-001-5.json')

  def test_excl_001_6(self):
    _test_from_file.run_test_spec(self, 'test_excluded/excl-001-6.json')

