# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_from_file


class TestProtected(unittest.TestCase):
  """Test Meeks protected candidates from file-based specs"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = 800

  def test_prot_001_1(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-001-1.json')

  def test_prot_001_2(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-001-2.json')

  def test_prot_001_3(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-001-3.json')

  def test_prot_001_4(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-001-4.json')

  def test_prot_001_5(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-001-5.json')

  def test_prot_002_1(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-002-1.json')

  def test_prot_002_2(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-002-2.json')

  def test_prot_002_3(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-002-3.json')

  def test_prot_003_1(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-003-1.json')

  def test_prot_003_2(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-003-2.json')

  def test_prot_003_3(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-003-3.json')

  def test_prot_004_1(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-004-1.json')

  def test_prot_004_2(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-004-2.json')

  def test_prot_004_3(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-004-3.json')

  def test_prot_031_1(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-1.json')

  def test_prot_031_1_bsd(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-1-bsd.json')

  def test_prot_031_1_inne(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-1-inne.json')

  def test_prot_031_1_prr(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-1-prr.json')

  def test_prot_031_2(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-2.json')

  def test_prot_031_2_bsd(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-2-bsd.json')

  def test_prot_031_2_inne(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-2-inne.json')

  def test_prot_031_2_prr(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-031-2-prr.json')

  def test_prot_032_1(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-1.json')

  def test_prot_032_1_bsd(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-1-bsd.json')

  def test_prot_032_1_inne(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-1-inne.json')

  def test_prot_032_1_prr(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-1-prr.json')

  def test_prot_032_2(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-2.json')

  def test_prot_032_2_bsd(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-2-bsd.json')

  def test_prot_032_2_inne(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-2-inne.json')

  def test_prot_032_2_prr(self):
    _test_from_file.run_test_spec(self, 'test_protected/prot-032-2-prr.json')

