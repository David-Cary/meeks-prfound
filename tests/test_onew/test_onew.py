# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_from_file


class TestOneW(unittest.TestCase):
  """Test for one winner from file-based specs"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = 800

  def test_onew_001(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-001.json')


  def test_onew_002(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-002.json')

  def test_onew_003(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-003.json')

  def test_onew_004(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-004.json')

  def test_onew_005(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-005.json')

  def test_onew_005_aa(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-005-aa.json')

  def test_onew_005_oo(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-005-oo.json')

  def test_onew_005_ta(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-005-ta.json')

  def test_onew_005_to(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-005-to.json')

  def test_onew_005_t1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-005-t1.json')

  def test_onew_006_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-006-1.json')

  def test_onew_006_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-006-2.json')

  def test_onew_007_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-007-1.json')

  def test_onew_007_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-007-2.json')

  def test_onew_007_3(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-007-3.json')

  def test_onew_007_4(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-007-4.json')

  def test_onew_008_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-008-1.json')

  def test_onew_008_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-008-2.json')

  def test_onew_008_3(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-008-3.json')

  def test_onew_008_4(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-008-4.json')

  def test_onew_009(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-009.json')

  def test_onew_009_default(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-009-default.json')

  def test_onew_009_prr(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-009-prr.json')

  def test_onew_009_bsd(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-009-bsd.json')

  def test_onew_009_inne(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-009-inne.json')

  def test_onew_010_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-1.json')

  def test_onew_010_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-2.json')

  def test_onew_010_3(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-3.json')

  def test_onew_010_4(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-4.json')

  def test_onew_010_prr(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-prr.json')

  def test_onew_010_bsd(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-bsd.json')

  def test_onew_010_inne(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-010-inne.json')

  def test_onew_011_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-011-1.json')

  def test_onew_011_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-011-2.json')

  def test_onew_012_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-012-1.json')

  def test_onew_012_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-012-2.json')

  def test_onew_012_5(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-012-5.json')

  def test_onew_012_prr(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-012-prr.json')

  def test_onew_012_bsd(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-012-bsd.json')

  def test_onew_012_inne(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-012-inne.json')

  def test_onew_013_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-013-1.json')

  def test_onew_013_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-013-2.json')

  def test_onew_014_1(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-014-1.json')

  def test_onew_014_2(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-014-2.json')

  def test_onew_014_3(self):
    _test_from_file.run_test_spec(self, 'test_onew/onew-014-3.json')

