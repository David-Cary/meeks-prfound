# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_aids

from _src import meeks_prfound
from meeks_prfound import decimal9

ONECK = 1000000000
D9 = decimal9.Decimal9
D9TOTAL = decimal9.Decimal9Total

def test_compare(test_case, x, y, comparison):
  """Test all order comparisons for a pair of values"""
  test_case.assertEqual(x.__lt__(y), comparison < 0)
  test_case.assertEqual(x < y, comparison < 0)
  test_case.assertEqual(x.__le__(y), comparison <= 0)
  test_case.assertEqual(x <= y, comparison <= 0)
  test_case.assertEqual(x.__eq__(y), comparison == 0)
  test_case.assertEqual(x == y, comparison == 0)
  test_case.assertEqual(x.__ne__(y), comparison != 0)
  test_case.assertEqual(x != y, comparison != 0)
  test_case.assertEqual(x.__ge__(y), comparison >= 0)
  test_case.assertEqual(x >= y, comparison >= 0)
  test_case.assertEqual(x.__gt__(y), comparison > 0)
  test_case.assertEqual(x > y, comparison > 0)

class TestBasicDecimal9(unittest.TestCase):
  """Test basic Decimal9"""

  def test_decimal9_creation_with_ints(self):
    self.assertEqual(D9()._get_value(), 0)
    self.assertEqual(D9(1)._get_value(), 1 * ONECK)
    self.assertEqual(D9(2)._get_value(), 2 * ONECK)
    self.assertEqual(D9(-1)._get_value(), -1 * ONECK)
    self.assertEqual(D9(10)._get_value(), 10 * ONECK)
    self.assertEqual(D9(-123)._get_value(), -123 * ONECK)
    self.assertEqual(D9(9, 0)._get_value(), 9 * ONECK)
    self.assertEqual(D9(11, -5)._get_value(), 110000)
    self.assertEqual(D9(7, -3)._get_value(), 7000000)
    self.assertEqual(D9(7, -9)._get_value(), 7)
    self.assertEqual(D9(1000000000, -9)._get_value(), 1 * ONECK)
    self.assertEqual(D9(98765, -7)._get_value(), 9876500)
    self.assertEqual(D9(-98765, -7)._get_value(), -9876500)
    self.assertEqual(D9(65432, -7)._get_value(), 6543200)
    self.assertEqual(D9(-65432, -7)._get_value(), -6543200)
    self.assertEqual(D9(98765, -11)._get_value(), 987)
    self.assertEqual(D9(-98765, -11)._get_value(), -987)
    self.assertEqual(D9(65432, -11)._get_value(), 654)
    self.assertEqual(D9(-65432, -11)._get_value(), -654)
    self.assertEqual(D9(721, 2)._get_value(), 72100000000000)
    self.assertEqual(D9(-721, 2)._get_value(), -72100000000000)
    self.assertEqual(D9(-123, 2.4)._get_value(), -12300 * ONECK)
    self.assertEqual(D9(-123, 2.6)._get_value(), -123000 * ONECK)


  def test_decimal9_creation_with_longs(self):
    self.assertEqual(D9(987654321987654321)._get_value(),
          987654321987654321 * ONECK)
    self.assertEqual(D9(-987654321987654321)._get_value(),
          -987654321987654321 * ONECK)
    self.assertEqual(D9(987654321987654321, -2)._get_value(),
          9876543219876543210000000)
    self.assertEqual(D9(-987654321987654321, -2)._get_value(),
          -9876543219876543210000000)
    self.assertEqual(D9(987654321987654321, -5)._get_value(),
          9876543219876543210000)
    self.assertEqual(D9(-987654321987654321, -5)._get_value(),
          -9876543219876543210000)
    self.assertEqual(D9(987654321987654321, -7)._get_value(),
          98765432198765432100)
    self.assertEqual(D9(-987654321987654321, -7)._get_value(),
          -98765432198765432100)
    self.assertEqual(D9(987654321987654321, -11)._get_value(),
          9876543219876543)
    self.assertEqual(D9(-987654321987654321, -11)._get_value(),
          -9876543219876543)


  def test_decimal9_creation_with_floats(self):
    self.assertEqual(D9(1.7,)._get_value(), 1700000000)
    self.assertEqual(D9(1.2345678)._get_value(), 1234567800)
    self.assertEqual(D9(-0.00237)._get_value(), -2370000)
    self.assertEqual(D9(9876.54321987654321)._get_value(),
          9876543219877)
    self.assertEqual(D9(98765.4321987654321)._get_value(),
          98765432198765)

  def test_decimal9_creation_with_string(self):
    _test_aids.assertRaises_with_message(self,
          decimal9.Decimal9Error,
          'Value is not a supported type:',
          D9, ('2.8',))

  def test_decimal9_creation_with_list(self):
    _test_aids.assertRaises_with_message(self,
          decimal9.Decimal9Error,
          'Value is not a supported type:',
          D9, ([-3.9],))

  def test_decimal9_to_str(self):
    self.assertEqual(str(D9()), '0.000000000')
    self.assertEqual(str(D9(1)), '1.000000000')
    self.assertEqual(str(D9(-21)), '-21.000000000')
    self.assertEqual(str(D9(1, -5)), '0.000010000')
    self.assertEqual(str(D9(-1, -5)), '-0.000010000')
    self.assertEqual(str(D9(238, -4)), '0.023800000')
    self.assertEqual(str(D9(238, -2)), '2.380000000')
    self.assertEqual(str(D9(987654321987654321)),
          '987654321987654321.000000000')
    self.assertEqual(str(D9(-987654321987654321)),
          '-987654321987654321.000000000')
    self.assertEqual(str(D9(987654321987654321, -5)),
          '9876543219876.543210000')
    self.assertEqual(str(D9(-987654321987654321, -5)),
          '-9876543219876.543210000')
    self.assertEqual(str(D9(-987654321987654321, -11)),
          '-9876543.219876543')


  def test_decimal9_to_repr(self):
    self.assertEqual(repr(D9(27182818, -7)), '2.718281800')

  def test_decimal9_to_from_float(self):
    # IEEE 754 double precision (64 bit) floating point numbers can
    # exactly represent all integers between 0 and
    # 9,007,199,254,740,992.  The following tests demonstrate that this
    # is also approximately where the precision of Python's conversion
    # of floats to and from strings of floats with 9 decimals of
    # precision begins to break down.  As a result, this is also where
    # the precision of conversion of Decimal9 values to and from JSON
    # numeric values begins to fail.

    self.assertEqual(repr(9007199.254740981), '9007199.254740981')
    self.assertEqual(repr(9007199.254740983), '9007199.254740983')
    self.assertEqual(repr(9007199.254740985), '9007199.254740985')
    self.assertEqual(repr(9007199.254740987), '9007199.254740987')
    self.assertEqual(repr(9007199.254740989), '9007199.254740989')
    self.assertNotEqual(repr(9007199.254740991), '9007199.254740991')
    self.assertEqual(repr(9007199.254740991), '9007199.25474099')
    self.assertEqual(repr(9007199.254740993), '9007199.254740993')
    self.assertEqual(repr(9007199.254740995), '9007199.254740994')
    self.assertNotEqual(repr(9007199.254740995), '9007199.254740995')
    self.assertEqual(repr(float('9007199.254740993')), '9007199.254740993')
    self.assertEqual(repr(float('9007199.254740995')), '9007199.254740994')
    self.assertNotEqual(repr(float('9007199.254740995')), '9007199.254740995')
    self.assertEqual(float('9007199.254740995'), float('9007199.254740994'))
    #self.assertEqual(repr(9007199254740993.0), '9007199254740993.0')

    self.assertEqual(repr(D9(9007199.254740991)), '9007199.254740991')
    self.assertEqual(repr(D9(9007199254740991, -9)), '9007199.254740991')
    self.assertEqual(float(repr(D9(9007199254740989, -9))), 9007199.254740989)
    self.assertEqual(repr(float(repr(D9(9007199254740989, -9)))),
          '9007199.254740989')
    self.assertEqual(float(repr(D9(9007199254740991, -9))), 9007199.254740991)
    self.assertEqual(repr(float(repr(D9(9007199254740991, -9)))),
          '9007199.25474099')
    self.assertNotEqual(repr(float(repr(D9(9007199254740991, -9)))),
          '9007199.254740991')
    self.assertEqual(repr(D9(float('9007199.254740991'))), '9007199.254740991')
    self.assertEqual(repr(D9(float('9007199.254740993'))), '9007199.254740992')
    self.assertNotEqual(repr(D9(float('9007199.254740993'))),
          '9007199.254740993')


  def test_decimal9_compare(self):
    test_compare(self, D9(), D9(), 0)
    test_compare(self, D9(), D9(0), 0)
    test_compare(self, D9(-1), D9(-1), 0)
    test_compare(self, D9(-3), D9(-2), -1)
    test_compare(self, D9(798, -9), D9(7987, -10), 0)
    test_compare(self, D9(798, -5), D9(7987, -6), -1)
    test_compare(self, D9(12) , D9(2), 1)
    test_compare(self, D9(122), D9(29), 1)
    self.assertFalse(D9() == None)
    _test_aids.assertRaises_with_message(self,
          meeks_prfound.decimal9.Decimal9Error,
          'Value is not an instance of the required type:',
          D9().__eq__, (0,))
    _test_aids.assertRaises_with_message(self,
          meeks_prfound.decimal9.Decimal9Error,
          'Value is not an instance of the required type:',
          (lambda x, y: x < y), (D9(1), 7))


  def test_decimal9_add(self):
    self.assertEqual(D9(3).__add__(D9(4))._get_value(), 7000000000)
    self.assertEqual(D9(3) + D9(9), D9(12))
    self.assertEqual(D9(314159, -5) + D9(314159, -5), D9(628318, -5))
    self.assertEqual(D9(314159, -5) + D9(-314159, -5), D9())


  def test_decimal9_sub(self):
    self.assertEqual(D9(3).__sub__(D9(4))._get_value(), -1000000000)
    self.assertEqual(D9(3) - D9(9), D9(-6))
    self.assertEqual(D9(314159, -5) - D9(-314159, -5), D9(628318, -5))
    self.assertEqual(D9(314159265358, -11) - D9(-314159265358, -11),
          D9(6283185306, -9))
    self.assertEqual(D9(314159265358, -11) - D9(314159265363, -11), D9())

  def test_decimal9_neg(self):
    self.assertEqual(D9(3).__neg__()._get_value(), -3000000000)
    self.assertEqual(-D9(-3), D9(3))
    self.assertEqual(-D9(314159265358, -5), D9(-314159265358, -5))
    self.assertEqual(-D9(), D9())

  def test_decimal9_mul(self):
    self.assertEqual(D9(3).__mul__(D9(4))._get_value(), 12000000000)
    self.assertEqual(D9(3) * D9(-9), D9(-27))
    self.assertEqual(D9(-314159, -5) * D9(-314159, -5), D9(9869587728, -9))
    self.assertEqual(D9(-3141592653, -9) * D9(-3141592653, -9),
          D9(9869604397, -9))
    self.assertEqual(D9(3).__mul__(4)._get_value(), 12000000000)
    self.assertEqual(D9(3) * -9, D9(-27))
    two_thirds = D9(2)/D9(3)
    self.assertEqual(two_thirds, D9(666666666, -9))

    eight_ninths = D9(8)/D9(9)
    self.assertEqual(two_thirds.multiply_by(two_thirds), D9(444444443, -9))
    self.assertEqual(two_thirds.multiply_by(-two_thirds), D9(-444444443, -9))
    self.assertEqual(two_thirds.multiply_by(two_thirds, True),
          D9(444444444, -9))
    self.assertEqual(two_thirds.multiply_by(-two_thirds, True),
          D9(-444444444, -9))
    self.assertEqual(two_thirds.multiply_by(2), D9(1333333332, -9))
    self.assertEqual(two_thirds.multiply_by(2, True), D9(1333333332, -9))
    self.assertEqual(two_thirds.multiply_by(-2), D9(-1333333332, -9))
    self.assertEqual(two_thirds.multiply_by(-2, True), D9(-1333333332, -9))

  def test_decimal9_div(self):
    self.assertEqual(D9(24).__div__(D9(3))._get_value(), 8000000000)
    self.assertEqual(D9(2) / D9(3), D9(666666666, -9))
    self.assertEqual(D9(-2) / D9(3), D9(-666666666, -9))
    self.assertEqual(D9(2) / D9(-3), D9(-666666666, -9))
    self.assertEqual(D9(-2) / D9(-3), D9(666666666, -9))
    self.assertEqual(D9(2) / 3, D9(666666666, -9))
    self.assertEqual(D9(2).divide_by(3, round_away=True), D9(666666667, -9))
    self.assertEqual(D9(2).__div__(D9(3), round_away=True),
          D9(666666667, -9))
    self.assertEqual(D9(-2).__div__(D9(3), round_away=True),
          D9(-666666667, -9))
    self.assertEqual(D9(-3141592653, -9) / D9(-3141592653, -9), D9(1))
    self.assertEqual(D9(2).divide_by(D9(-3)), D9(-666666666, -9))
    self.assertEqual(D9(2).divide_by(D9(-3), round_away=True),
          D9(-666666667, -9))
    _test_aids.assertRaises_with_message(self,
          ZeroDivisionError,
          ('integer division or modulo by zero',
          'long division or modulo by zero'),
          D9.divide_by, (D9(2), D9()))


  def test_decimal9total_create(self):
    self.assertEqual(D9TOTAL()._get_value(), 0)

  def test_decimal9total_add(self):
    self.assertEqual(D9TOTAL().__add__(D9(4))._get_value(), 4000000000)
    self.assertEqual(D9TOTAL() + D9(3) + D9(9), D9(12))
    self.assertEqual(D9TOTAL() + D9(314159, -5) + D9(314159, -5),
          D9(628318, -5))
    self.assertEqual(D9TOTAL() + D9(314159, -5) + D9(-314159, -5), D9())
    self.assertEqual((D9TOTAL() + D9(3)) + (D9TOTAL() + D9(-8)), D9(-5))

  def test_decimal9total_sub(self):
    self.assertEqual(D9TOTAL().__sub__(D9(4))._get_value(), -4000000000)
    self.assertEqual((D9TOTAL() + D9(3)).__sub__(D9(4))._get_value(),
          -1000000000)
    self.assertEqual(D9TOTAL() + D9(4).__sub__(D9(3)), D9(1))
    self.assertEqual(D9TOTAL() + D9(3) - D9(9), D9(-6))
    self.assertEqual(D9TOTAL() + D9(314159, -5) - D9(-314159, -5),
          D9(628318, -5))
    self.assertEqual(D9(3141592653, -9) - D9(3141592653, -9), D9())
    self.assertEqual((D9TOTAL() + D9(3)) - (D9TOTAL() + D9(-8)), D9(11))


  def test_confirm_types(self):
    self.assertTrue(decimal9._confirm_types(28, int, str))
    self.assertTrue(decimal9._confirm_types('abc', str))
    self.assertTrue(decimal9._confirm_types(D9TOTAL(), D9))
    _test_aids.assertRaises_with_message(self,
          meeks_prfound.decimal9.Decimal9Error,
          'Value is not an instance of the required type:',
          decimal9._confirm_types, (3.4, int, str))


  def test_div_to_int(self):
    self.assertEqual(decimal9.div_to_int(6, 3), 2)
    self.assertEqual(decimal9.div_to_int(6, 3, round_away=True), 2)
    self.assertEqual(decimal9.div_to_int(7, 3), 2)
    self.assertEqual(decimal9.div_to_int(7, 3, round_away=True), 3)
    self.assertEqual(decimal9.div_to_int(8, 3), 2)
    self.assertEqual(decimal9.div_to_int(8, 3, round_away=True), 3)
    self.assertEqual(decimal9.div_to_int(-7, 3), -2)
    self.assertEqual(decimal9.div_to_int(-7, 3, round_away=True), -3)
    self.assertEqual(decimal9.div_to_int(-8, 3), -2)
    self.assertEqual(decimal9.div_to_int(-8, 3, round_away=True), -3)
    self.assertEqual(decimal9.div_to_int(7, -3), -2)
    self.assertEqual(decimal9.div_to_int(7, -3, round_away=True), -3)
    self.assertEqual(decimal9.div_to_int(8, -3), -2)
    self.assertEqual(decimal9.div_to_int(8, -3, round_away=True), -3)
    self.assertEqual(decimal9.div_to_int(-7, -3), 2)
    self.assertEqual(decimal9.div_to_int(-7, -3, round_away=True), 3)
    self.assertEqual(decimal9.div_to_int(-8, -3), 2)
    self.assertEqual(decimal9.div_to_int(-8, -3, round_away=True), 3)

