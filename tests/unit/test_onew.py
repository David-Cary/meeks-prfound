# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_aids

from _src import meeks_prfound
from meeks_prfound import meek
from meeks_prfound import constants as K

class TestOneW(unittest.TestCase):
  """Test for one winner"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = 800

  def test_onew_01(self):
    candidates = ' A B C'
    ballots = (
          (15, ' A B C'),
          (10, ' B C A'),
          (8,  ' C B A'),
          )
    tie_breaker = ' A B C'
    options = {}
    exp_elected = ('B',)
    exp_status = _test_aids.build_expected_status((
          ('A', 'defeated', 2, K.ONE * 15, K.ONE),
          ('B', 'elected',  2, K.ONE * 18, K.ONE),
          ('C', 'defeated', 1, K.ONE *  8, K.ZERO),
          ))
    exp_tally = _test_aids.build_stv_tally({
          'A': [15.0, 15.0],
          'B': [10.0, 18.0],
          'C': [8.0],
          ':Overvotes': [0.0, 0.0],
          ':Abstentions': [0.0, 0.0],
          ':Other exhausted': [0.0, 0.0],
          ':Votes for candidates': [33.0, 33.0],
          ':Total votes': [33.0, 33.0],
          ':Quota': [16.500000001, 16.500000001],
          ':Total surplus': [0.0, 1.499999999],
          ':Iterations': [1, 1]
          })
    elected, status, tally = meek.Tabulation(1, candidates, ballots,
          3, tie_breaker, tuple(), tuple(), options).tabulate()
    status_dict = {candidate: status.as_dict()
          for candidate, status in status.items()}
    self.assertEqual(set(elected), set(exp_elected))
    self.assertEqual(status_dict, exp_status)
    self.assertEqual(tally, exp_tally)

  def test_onew_02(self):
    candidates = ' A B C D'
    ballots = (
          (15, ' A B C'),
          (8, ' B C D'),
          (1,  ' B'),
          (1,  ' B #'),
          (8,  ' C B A'),
          (5,  ' D C B'),
          )
    tie_breaker = ' A B C D'
    options = {}
    exp_elected = ('C',)
    exp_status = _test_aids.build_expected_status((
          ('A', 'defeated', 3, K.ONE * 15, K.ONE),
          ('B', 'defeated',  2, K.ONE * 10, K.ZERO),
          ('C', 'elected', 3, K.ONE * 21, K.ONE),
          ('D', 'defeated',  1, K.ONE * 5, K.ZERO),
          ))
    exp_tally = _test_aids.build_stv_tally({
          'A': [15, 15, 15],
          'B': [10, 10],
          'C': [8, 13, 21],
          'D': [5],
          ':Overvotes': [0, 0, 1],
          ':Abstentions': [0, 0, 1],
          ':Other exhausted': [0, 0, 0],
          ':Votes for candidates': [38.0, 38.0, 36.0],
          ':Total votes': [38.0, 38.0, 38.0],
          ':Quota': [19.000000001, 19.000000001, 18.000000001],
          ':Total surplus': [0.0, 0.0, 2.999999999],
          ':Iterations': [1, 1, 1]
          })
    elected, status, tally = meek.Tabulation(1, candidates, ballots,
          3, tie_breaker, tuple(), tuple(), options).tabulate()
    status_dict = {candidate: status.as_dict()
          for candidate, status in status.items()}
    self.assertEqual(set(elected), set(exp_elected))
    self.assertEqual(status_dict, exp_status)
    self.assertEqual(tally, exp_tally)
