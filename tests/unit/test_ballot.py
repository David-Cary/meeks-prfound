# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest

from _src import meeks_prfound
from meeks_prfound import ballot


class TestBallot(unittest.TestCase):
  """Test the ballot module and Ballot class"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff

  def tearDown(self):
    self.maxDiff = self.save_maxDiff

  def get_exception_message(self, exception_type, args):
    """Get an exception's string representation"""
    message = None
    try:
      raise exception_type(*args)
    except exception_type as exc:
      message = str(exc)
    return message

  def test_ballot_create(self):
    test_ballot = ballot.Ballot(3, ('A', 'B', 'C'))
    self.assertEqual(test_ballot._multiple, 3)
    self.assertEqual(test_ballot._rankings, ('A', 'B', 'C'))

  def test_ballot_accessors(self):
    test_ballot = ballot.Ballot(7, ('C', 'B', 'A'))
    self.assertEqual(test_ballot.get_multiple(), 7)
    self.assertEqual(test_ballot.get_rankings(), ('C', 'B', 'A'))

  def test_ballot_as_string(self):
    test_ballot = ballot.Ballot(7, ('C', 'B', 'A'))
    self.assertEqual(repr(test_ballot), "(7, ('C', 'B', 'A'))")
    self.assertEqual(str(test_ballot), "(7, ('C', 'B', 'A'))")


