# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_aids

from _src import meeks_prfound
from meeks_prfound import meek

class TestMeeks(unittest.TestCase):
  """Test Meeks"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = 4000

  def test_meeks_01(self):
    candidates = ' A B C D E'
    ballots = (
          (15, ' A B C D E'),
          (7,  ' B C A E D'),
          (2,  ' C B A D E'),
          (7,  ' D E A B C'),
          (9,  ' E D C B A'),
          )
    tie_breaker = ' A B C E D'
    options = {}
    exp_elected = ('A', 'B', 'E')
    exp_status = _test_aids.build_expected_status((
          ('A', 'elected', 1, 15.0, 0.556687034),
          ('B', 'elected',  2, 11.999999995, 0.646687698),
          ('C', 'defeated', 3, 3.999999987, 0.0),
          ('D', 'defeated', 4, 9.662674708, 1.0),
          ('E', 'elected', 4, 10.096395468, 1.0)
          ))
    exp_tally = _test_aids.build_stv_tally({
          'A': [15.0, 10.000000005, 10.000000005, 10.120464912],
          'B': [7.0, 11.999999995, 10.000000008, 10.120464912],
          'C': [2.0, 2.0, 3.999999987],
          'D': [7.0, 7.0, 7.0, 9.662674708],
          'E': [9.0, 9.0, 9.0, 10.096395468],
          ':Overvotes': [0.0, 0.0, 0.0, 0.0],
          ':Abstentions': [0.0, 0.0, 0.0, 0.0],
          ':Other exhausted': [0.0, 0.0, 0.0, 0.0],
          ':Votes for candidates': [40.0, 40.0, 40.0, 40.0],
          ':Total votes': [40.0, 40.0, 40.0, 40.0],
          ':Quota': [10.000000001, 10.000000001, 10.000000001, 10.000000001],
          ':Total surplus': [4.999999999, 1.999999998, 1.1e-08, 0.337325289],
          ':Iterations': [1, 2, 2, 4]
          })
    elected, status, tally = meek.Tabulation(3, candidates, ballots,
          5, tie_breaker, tuple(), tuple(), options).tabulate()
    status_dict = {candidate: status.as_dict()
          for candidate, status in status.items()}
    self.assertEqual(set(elected), set(exp_elected))
    self.assertEqual(status_dict, exp_status)
    self.assertEqual(tally, exp_tally)


  def test_meeks_02(self):
    candidates = '|Lock|Lights|Bike Computer|Patch Kit|Tire Pump|Helmet|Bell|Mirror|Rack|Fenders'
    ballots = (
          (1, '|Lights|Lock|Rack|Bell|Bike Computer|Tire Pump|Patch Kit|Fenders|Helmet|Mirror'),
          (1, '|Lights|Tire Pump|Bike Computer|Helmet|Bell|Lock|Rack|Patch Kit|Fenders|Mirror'),
          (1, '|Helmet|Lights|Lock|Patch Kit|Tire Pump|Rack|Bell|Bike Computer|Fenders|Mirror'),
          (1, '|Helmet|Lights|Bell|Lock|Mirror|Bike Computer||||'),
          (1, '|Bell|Lights|Helmet|Lock|Bike Computer|||||'),
          (1, '|Helmet|Bell|Patch Kit|Lock|Rack|Lights|Mirror|Bike Computer|Fenders|Tire Pump'),
          (1, '|Helmet|Mirror|Lock|Lights|Patch Kit|Tire Pump|Rack|Fenders|Bell|Bike Computer'),
          (1, '|Lock|Helmet|Lights|Rack|Bell|Mirror|Fenders|Tire Pump|Patch Kit|Bike Computer'),
          (1, '|Helmet|Lock|Lights|Bell|Rack|Patch Kit|Tire Pump|Mirror|Fenders|Bike Computer'),
          (1, '|Lock|Lights|Fenders|Rack|Tire Pump|Patch Kit|||Mirror|Bike Computer'),
          )

    tie_breaker = '|Tire Pump|Rack|Lights|Bell|Mirror|Bike Computer|Helmet|Fenders|Patch Kit|Lock'
    options = {}
    exp_elected = ('Helmet', 'Lights', 'Lock', 'Bell', 'Rack')
    exp_status = _test_aids.build_expected_status((
          ('Helmet', 'elected', 1, 5.0, 0.277431723),
          ('Lights', 'elected', 1, 2.0, 0.281481953),
          ('Lock', 'elected', 1, 2.0, 0.311956109),
          ('Bell', 'elected', 2, 1.796296294, 0.643793424),
          ('Rack', 'elected', 7, 1.650295804, 1.0),
          ('Tire Pump', 'defeated', 7, 1.432953025, 1.0),
          ('Mirror', 'defeated', 6, 0.829464938, 0.0),
          ('Patch Kit', 'defeated', 5, 0.547571763, 0.0),
          ('Fenders', 'defeated', 4, 0.419393612, 0.0),
          ('Bike Computer', 'defeated', 3, 0.097371969, 0.0),
          ))
    exp_tally = _test_aids.build_stv_tally({
          'Helmet': [5.0, 1.722222226, 1.666666729, 1.649436679, 1.649436679, 1.648330517, 1.649049930],
          'Lights': [2.0, 3.101851853, 1.666666815, 1.649436756, 1.649436756, 1.648330646, 1.683505025],
          'Lock': [2.0, 2.453703705, 1.666666806, 1.649436756, 1.649436756, 1.648330682, 1.656513773],
          'Bell': [1.0, 1.796296294, 1.666666791, 1.649436742, 1.649436742, 1.648330618, 1.673196007],
          'Rack': [0.0, 0.046296295, 0.801966117, 0.823846149, 1.243239761, 1.460919757, 1.650295804],
          'Tire Pump': [0.0, 0.166666666, 0.681050552, 0.686805846, 0.686805846, 1.006275255, 1.432953025],
          'Mirror': [0.0, 0.666666666, 0.811469017, 0.821255352, 0.821255352, 0.829464938],
          'Patch Kit': [0.0, 0.018518518, 0.530416712, 0.547571763, 0.547571763],
          'Fenders': [0.0, 0.027777777, 0.411058492, 0.419393612],
          'Bike Computer': [0.0, 0.0, 0.097371969],
          ':Overvotes': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          ':Abstentions': [0.0, 0.0, 0.0, 0.103380345, 0.103380345, 0.110017587, 0.254486436],
          ':Other exhausted': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          ":Votes for candidates": [10.0, 10.0, 10.0, 9.896619655, 9.896619655,
              9.889982413, 9.745513564],
          ":Total votes": [10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0],
          ":Quota": [1.666666667, 1.666666667, 1.666666667, 1.64943661,
              1.64943661, 1.648330403, 1.624252261],
          ":Total surplus": [3.999999999, 2.407407410, 4.73e-07, 4.93e-07, 4.93e-07,
              8.51e-07, 0.191299234],
          ":Iterations": [1, 2, 17, 14, 1, 14, 2]
          })
    elected, status, tally = meek.Tabulation(5, candidates, ballots,
          10, tie_breaker, tuple(), tuple(), options).tabulate()
    status_dict = {candidate: status.as_dict()
          for candidate, status in status.items()}
    self.assertEqual(set(elected), set(exp_elected))
    self.assertEqual(status_dict, exp_status)
    self.assertEqual(tally, exp_tally)

