# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import sys

import unittest
import _test_aids

from _src import meeks_prfound
from meeks_prfound import validate
from meeks_prfound import errors
from meeks_prfound.constants import Decimal
ONE = Decimal(1)

def sample_options_function(parms, result):
  return 'Y'

class TestValidate(unittest.TestCase):
  """Test the validate module"""

  def test_str_tuple_from_str(self):
    self.assertEqual(validate.str_tuple(''), tuple())
    self.assertEqual(validate.str_tuple(' '), ('',))
    self.assertEqual(validate.str_tuple('#'), ('',))
    self.assertEqual(validate.str_tuple(' A'), ('A',))
    self.assertEqual(validate.str_tuple(' AA'), ('AA',))
    self.assertEqual(validate.str_tuple(' A B'), ('A', 'B'))
    self.assertEqual(validate.str_tuple(' A  B'), ('A', '', 'B'))
    self.assertEqual(validate.str_tuple('|A|B'), ('A', 'B'))

  def test_str_tuple_from_list(self):
    self.assertEqual(validate.str_tuple([]), tuple())
    self.assertEqual(validate.str_tuple(['']), ('',))
    self.assertEqual(validate.str_tuple(['#']), ('#',))
    self.assertEqual(validate.str_tuple(['AA']), ('AA',))
    self.assertEqual(validate.str_tuple(['A', 'B']), ('A', 'B'))
    self.assertEqual(validate.str_tuple(['A', '', 'B']), ('A', '', 'B'))
    _test_aids.assertRaises_with_message(self, TypeError,
          'Item in list is not a str:',
          validate.str_tuple, (['A', 7, 'B'],))

  def test_str_tuple_from_tuple(self):
    self.assertEqual(validate.str_tuple(tuple()), tuple())
    self.assertEqual(validate.str_tuple(('',)), ('',))
    self.assertEqual(validate.str_tuple(('#',)), ('#',))
    self.assertEqual(validate.str_tuple(('AA',)), ('AA',))
    self.assertEqual(validate.str_tuple(('A', 'B')), ('A', 'B'))
    self.assertEqual(validate.str_tuple(('A', '', 'B')), ('A', '', 'B'))
    _test_aids.assertRaises_with_message(self, TypeError,
          'Item in tuple is not a str:',
          validate.str_tuple, (('A', 7, 'B'),))

  def test_str_tuple_from_other(self):
    type_type = 'type' if sys.version_info.major == 2 else 'class'
    _test_aids.assertRaises_with_message(self, TypeError,
          'Can not make a str_tuple from a <' + type_type + ' \'set\'>.',
          validate.str_tuple, (set(('A', 7, 'B')),))

  def test_nbr_seats_to_fill_valid(self):
    self.assertEqual(validate.nbr_seats_to_fill(1), 1)
    self.assertEqual(validate.nbr_seats_to_fill(2), 2)
    self.assertEqual(validate.nbr_seats_to_fill(3), 3)
    self.assertEqual(validate.nbr_seats_to_fill(4), 4)
    self.assertEqual(validate.nbr_seats_to_fill(5), 5)
    self.assertEqual(validate.nbr_seats_to_fill(1020), 1020)

  def test_nbr_seats_to_fill_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'nbr_seats_to_fill not an int:',
          validate.nbr_seats_to_fill, ('two',))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'nbr_seats_to_fill not >= 1:',
          validate.nbr_seats_to_fill, (0,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'nbr_seats_to_fill not >= 1:',
          validate.nbr_seats_to_fill, (-1,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'nbr_seats_to_fill not >= 1:',
          validate.nbr_seats_to_fill, (-2,))

  def test_candidates_valid(self):
    self.assertEqual(validate.candidates(()), ())
    self.assertEqual(validate.candidates([]), ())
    self.assertEqual(validate.candidates(''), ())
    self.assertEqual(validate.candidates(('A',)), ('A',))
    self.assertEqual(validate.candidates(('B', 'A')), ('B', 'A'))
    self.assertEqual(validate.candidates(('B', 'C', 'A')), ('B', 'C', 'A'))
    self.assertEqual(validate.candidates(
          ('B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L')),
          ('B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L'))
    self.assertEqual(validate.candidates(['A']), ('A',))
    self.assertEqual(validate.candidates(['B', 'A']), ('B', 'A'))
    self.assertEqual(validate.candidates(['B', 'C', 'A']), ('B', 'C', 'A'))
    self.assertEqual(validate.candidates(
          ['B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L']),
          ('B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L'))
    self.assertEqual(validate.candidates(' A'), ('A',))
    self.assertEqual(validate.candidates(' B A'), ('B', 'A'))
    self.assertEqual(validate.candidates(' B C A'), ('B', 'C', 'A'))
    self.assertEqual(validate.candidates(
          ' B C A I H G F E D J K L'),
          ('B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L'))
    self.assertEqual(validate.candidates(
          ',Joe Smith,John Doe'),
          ('Joe Smith', 'John Doe'))
    self.assertEqual(validate.candidates(
          ';Smith, Joe;Doe, John'),
          ('Smith, Joe', 'Doe, John'))

  def test_candidates_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidates type:',
          validate.candidates, (17,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name:',
          validate.candidates, (('A', ''),))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name:',
          validate.candidates, (('A', '#'),))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name:',
          validate.candidates, (('A', ':who'),))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Candidate names are not unique.',
          validate.candidates, (('A', 'B', 'A'),))

  def test_tie_breaker_valid(self):
    candidates = ('B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L',
          'Joe Smith', 'John Doe', 'Smith, Joe', 'Doe, John')
    self.assertEqual(validate.tie_breaker((), candidates), {})
    self.assertEqual(validate.tie_breaker([], candidates), {})
    self.assertEqual(validate.tie_breaker('', candidates), {})
    self.assertEqual(validate.tie_breaker(('A',), candidates), {'A': 0})
    self.assertEqual(validate.tie_breaker(
          ('B', 'A'), candidates),
          {'B': 0, 'A': 1})
    self.assertEqual(validate.tie_breaker(
          ('B', 'C', 'A'), candidates),
          {'B': 0, 'C': 1, 'A': 2})
    self.assertEqual(validate.tie_breaker(
          ('B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L'),
          candidates),
          {'B': 0, 'C': 1, 'A': 2, 'I': 3, 'H': 4, 'G': 5, 'F': 6, 'E': 7,
          'D': 8, 'J': 9, 'K': 10, 'L': 11})
    self.assertEqual(validate.tie_breaker(['A'], candidates), {'A': 0})
    self.assertEqual(validate.tie_breaker(
          ['B', 'A'], candidates),
          {'B': 0, 'A': 1})
    self.assertEqual(validate.tie_breaker(
          ['B', 'C', 'A'], candidates),
          {'B': 0, 'C': 1, 'A': 2})
    self.assertEqual(validate.tie_breaker(
          ['B', 'C', 'A', 'I', 'H', 'G', 'F', 'E', 'D', 'J', 'K', 'L'],
          candidates),
          {'B': 0, 'C': 1, 'A': 2, 'I': 3, 'H': 4, 'G': 5, 'F': 6, 'E': 7,
          'D': 8, 'J': 9, 'K': 10, 'L': 11})
    self.assertEqual(validate.tie_breaker(' A', candidates), {'A': 0})
    self.assertEqual(validate.tie_breaker(' B A', candidates),
          {'B': 0, 'A': 1})
    self.assertEqual(validate.tie_breaker(' B C A', candidates),
          {'B': 0, 'C': 1, 'A': 2})
    self.assertEqual(validate.tie_breaker(
          ' B C A I H G F E D J K L', candidates),
          {'B': 0, 'C': 1, 'A': 2, 'I': 3, 'H': 4, 'G': 5, 'F': 6, 'E': 7,
          'D': 8, 'J': 9, 'K': 10, 'L': 11})
    self.assertEqual(validate.tie_breaker(
          ',Joe Smith,John Doe', candidates),
          {'Joe Smith': 0, 'John Doe': 1})
    self.assertEqual(validate.tie_breaker(
          ';Smith, Joe;Doe, John', candidates),
          {'Smith, Joe': 0, 'Doe, John': 1})

  def test_tie_breaker_invalid(self):
    candidates = ('A', 'C')
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid tie_breaker type:',
          validate.tie_breaker, (17, candidates))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name in tie_breaker:',
          validate.tie_breaker, (('A', 'B'), candidates))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name in tie_breaker:',
          validate.tie_breaker, (('A', ''), candidates))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name in tie_breaker:',
          validate.tie_breaker, (('A', '#'), candidates))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name in tie_breaker:',
          validate.tie_breaker, (('A', ':who'), candidates))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Candidate names in tie_breaker are not unique.',
          validate.tie_breaker, (('A', 'C', 'A'), candidates))

  def test_ballots_valid(self):
    candidates = validate.str_tuple(' A B C D E F G H I J K L')
    self.assertEqual(validate.ballots((), candidates, 3), ())
    self.assertEqual(validate.ballots([], candidates, 3), ())
    self.assertEqual(repr(validate.ballots(
          ((5, ('A',)),), candidates, 3)),
          "((5, ('A',)),)")
    self.assertEqual(repr(validate.ballots(
          [[5, ['A',]],], candidates, 3)),
          "((5, ('A',)),)")
    self.assertEqual(repr(validate.ballots(
          [[5, ' A'],], candidates, 3)),
          "((5, ('A',)),)")
    self.assertEqual(repr(validate.ballots(
          [(3, ' A B'), (5, ' B A'),], candidates, 3)),
          "((3, ('A', 'B')), (5, ('B', 'A')))")
    self.assertEqual(repr(validate.ballots(
          [(3, ' A  B'), (5, ' B # A'),], candidates, 3)),
          "((3, ('A', '', 'B')), " +
          "(5, ('B', '#', 'A')))")
    self.assertEqual(repr(validate.ballots(
          [(3, ' A   B'), (5, '   A'),], candidates, 4)),
          "((3, ('A', '', '', 'B')), " +
          "(5, ('', '', 'A')))")
    self.assertEqual(repr(validate.ballots(
          [(3, ' A   B'), (5, '   A'),], candidates, None)),
          "((3, ('A', '', '', 'B')), " +
          "(5, ('', '', 'A')))")
    self.assertEqual(repr(validate.ballots(
          [(3, ''), (5, ' #'),], candidates, 3)),
          "((3, ()), (5, ('#',)))")
    self.assertEqual(repr(validate.ballots(
          [(3, ' A B C D E F G H I J K L'),
          (5, ' L K J I H G F E D C B A'),], candidates, 12)),
          "((3, ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'" +
          ", 'J', 'K', 'L'))," +
          " (5, ('L', 'K', 'J', 'I', 'H', 'G', 'F', 'E', 'D'" +
          ", 'C', 'B', 'A')))")
    self.assertEqual(repr(validate.ballots(
          [(3, ' A B C D E F G H I J K L'),
          (5, ' L K J I H G F E D C B A'),], candidates, None)),
          "((3, ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'" +
          ", 'J', 'K', 'L'))," +
          " (5, ('L', 'K', 'J', 'I', 'H', 'G', 'F', 'E', 'D'" +
          ", 'C', 'B', 'A')))")

  def test_ballots_invalid(self):
    candidates = validate.str_tuple(' A B C D E F G H I J K L')
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'ballots is not a list or tuple:',
          validate.ballots, (' A B', candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'A ballot is not a list or tuple:',
          validate.ballots, ((' A B',), candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'A ballot is not a pair of values:',
          validate.ballots, ([('A', 'B', 'C')], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'A ballot multiple is not an int:',
          validate.ballots, ([(0.12,' A B C')], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'A ballot multiple is zero or less:',
          validate.ballots, ([(-1,' A B C')], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'A ballot multiple is zero or less:',
          validate.ballots, ([(0,' A B C')], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid ballot rankings type:',
          validate.ballots, ([(1, 77)], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid ballot rankings type:',
          validate.ballots, ([(2, ('A', 77))], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Ballot rankings is too long:',
          validate.ballots, ([(2, ' A B C D')], candidates, 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid ballot ranking code:',
          validate.ballots, ([(2, ('A', 'Z'))], candidates, 3))

  def test_max_ranking_levels_valid(self):
    self.assertEqual(validate.max_ranking_levels(3), 3)
    self.assertEqual(validate.max_ranking_levels(4), 4)
    self.assertEqual(validate.max_ranking_levels(5), 5)
    self.assertEqual(validate.max_ranking_levels(1020), 1020)
    self.assertEqual(validate.max_ranking_levels(None), None)

  def test_max_ranking_levels_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'max_ranking_levels not an int:',
          validate.max_ranking_levels, ('two',))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'max_ranking_levels is less than 3:',
          validate.max_ranking_levels, (2,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'max_ranking_levels is less than 3:',
          validate.max_ranking_levels, (1,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'max_ranking_levels is less than 3:',
          validate.max_ranking_levels, (0,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'max_ranking_levels is less than 3:',
          validate.max_ranking_levels, (-1,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'max_ranking_levels is less than 3:',
          validate.max_ranking_levels, (-2,))

  def test_excluded_valid(self):
    self.assertEqual(validate.excluded(
          ' A B C', set(('A', 'B', 'C', 'D', 'E'))), set(('A', 'B', 'C')))
    self.assertEqual(validate.excluded(
          ('A', 'B'), set(('A', 'B', 'C', 'D', 'E'))), set(('A', 'B')))
    self.assertEqual(validate.excluded(None, set()), set())
    self.assertEqual(validate.excluded([], set()), set())
    self.assertEqual(validate.excluded('', set()), set())

  def test_excluded_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name in excluded:',
          validate.excluded, (' A', set()))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Candidate names in excluded are not unique:',
          validate.excluded, (' A B A', set(('A', 'B'))))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid excluded type:',
          validate.excluded, (3, set()))

  def test_protected_valid(self):
    self.assertEqual(validate.protected(
          ' A B C', set(('A', 'B', 'C', 'D', 'E')), set(('D',)), 3),
          set(('A', 'B', 'C')))
    self.assertEqual(validate.protected(
          ('A', 'B'), set(('A', 'B', 'C', 'D', 'E')), set(('D',)), 3),
          set(('A', 'B')))
    self.assertEqual(validate.protected(None, set(), set(), 3), set())
    self.assertEqual(validate.protected('', set(), set(), 3), set())
    self.assertEqual(validate.protected([], set(), set(), 3), set())

  def test_protected_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid candidate name in protected:',
          validate.protected, (' A', set(), set(), 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Candidate names in protected are not unique:',
          validate.protected, (' A B A', set(('A', 'B')), set(), 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid protected type:',
          validate.protected, (3, set(), set(), 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Candidate is protected and excluded:',
          validate.protected, (' A B', set(('A', 'B')), set(('A',)), 3))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'More protected candidates than there are seats to fill:',
          validate.protected, (' A B', set(('A', 'B')), set(), 1))

  def test_options_valid(self):
    self.assertEqual(validate.options({}), {})

  def test_options_altdefs_valid(self):
    self.assertEqual(validate.options(
          {'alternative_defeats': 'Y'}),
          {'alternative_defeats': 'Y'})
    self.assertEqual(validate.options(
          {'alternative_defeats': 'y'}),
          {'alternative_defeats': 'Y'})
    self.assertEqual(validate.options(
          {'alternative_defeats': 'N'}),
          {'alternative_defeats': 'N'})
    self.assertEqual(validate.options(
          {'alternative_defeats': ('y', 'n', 'y', 'Y')}),
          {'alternative_defeats': ('Y', 'N', 'Y', 'Y')})
    self.assertEqual(validate.options(
          {'alternative_defeats': ['y', 'n', 'N']}),
          {'alternative_defeats': ('Y', 'N', 'N')})
    self.assertEqual(validate.options(
          {'alternative_defeats': ' y n N Y'}),
          {'alternative_defeats': ('Y', 'N', 'N', 'Y')})

  def test_options_type_of_altdefs_valid(self):
    self.assertEqual(validate.options(
          {'type_of_altdefs': 'per_reference_rule'}),
          {'type_of_altdefs': 'per_reference_rule'})
    self.assertEqual(validate.options(
          {'type_of_altdefs': 'before_single_defeats'}),
          {'type_of_altdefs': 'before_single_defeats'})
    self.assertEqual(validate.options(
          {'type_of_altdefs': 'if_no_new_electeds'}),
          {'type_of_altdefs': 'if_no_new_electeds'})
    self.assertEqual(validate.options(
          {'type_of_altdefs': 'per_ReFerence_ruLe'}),
          {'type_of_altdefs': 'per_reference_rule'})

  def test_options_always_count_votes_valid(self):
    self.assertEqual(validate.options(
          {'always_count_votes': True}),
          {'always_count_votes': True})
    self.assertEqual(validate.options(
          {'always_count_votes': False}),
          {'always_count_votes': False})

  def test_options_combinations_valid(self):
    self.assertEqual(validate.options(
          {'alternative_defeats': 'N', 'always_count_votes': True}),
          {'alternative_defeats': 'N', 'always_count_votes': True})

  def test_options_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'options is not a dict:',
          validate.options, (7,))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'An option name is not a str:',
          validate.options, ({('junk',): True},))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid option name:',
          validate.options, ({'no_skipped_rankings': True},))

  def test_options_altdefs_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid option value type:',
          validate.options, ({'alternative_defeats': True},))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid per-round option value:',
          validate.options, ({'alternative_defeats': ('Z',)},))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid per-round option value:',
          validate.options, ({'alternative_defeats': ['Z']},))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid per-round option value:',
          validate.options, ({'alternative_defeats': ' Z'},))

  def test_options_type_of_altdefs_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid option value:',
          validate.options, ({'type_of_altdefs': 'Y'},))
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          'Invalid option value:',
          validate.options, ({'type_of_altdefs': 2},))

  def test_options_always_count_votes_invalid(self):
    _test_aids.assertRaises_with_message(self, errors.MeekValueError,
          "The option 'always_count_votes' must be True or False.",
          validate.options, ({'always_count_votes': 'Y'},))

