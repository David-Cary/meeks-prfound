# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

from __future__ import print_function

import unittest
import _test_aids

from _src import meeks_prfound
from meeks_prfound import meek
from meeks_prfound import ballot
from meeks_prfound import constants as K
from meeks_prfound import validate
from meeks_prfound.validate import str_tuple

import sys
import re

class TestMeekParts(unittest.TestCase):
  """Test individual Meek tabulation routines"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = None

  def tearDown(self):
    self.maxDiff = self.save_maxDiff

  def make_meek_01(self):
    candidates = ' A B C'
    ballots = (
          (15, ' A B C'),
          (10, ' B C A'),
          (8,  ' C B A'),
          )
    tie_breaker = ' A B C'
    options = {}
    return meek.Tabulation(1, candidates, ballots, 3, tie_breaker,
          tuple(), tuple(), options)

  def make_meek_02(self):
    candidates = ' A B C D'
    ballots = (
          (15, ' A B C'),
          (8, ' B C D'),
          (1,  ' B'),
          (1,  ' B #'),
          (8,  ' C B A'),
          (5,  ' D C B'),
          )
    tie_breaker = ' A B C D'
    options = {}
    return meek.Tabulation(1, candidates, ballots, 3, tie_breaker,
          tuple(), tuple(), options)

  def make_meek_03(self):
    candidates = ' A B C D'
    ballots = (
          (15, ' A B C'),
          (8, ' B C D'),
          (1,  ' B'),
          (1,  ' B #'),
          (8,  ' C B A'),
          (5,  ' D C B'),
          )
    tie_breaker = ' A B C D'
    options = {}
    return meek.Tabulation(3, candidates, ballots, 3, tie_breaker,
          tuple(), tuple(), options)

  def test_meek_01(self):
    self.maxDiff = None
    test_tabulation = self.make_meek_01()
    self.assertEqual(test_tabulation.nbr_seats_to_fill, 1)
    self.assertEqual(test_tabulation.candidates, ('A', 'B', 'C'))
    ballot0 = test_tabulation.ballots[0]
    ballots_as_tuples = tuple((ballot.as_tuple()
          for ballot in test_tabulation.ballots))
    self.assertEqual(ballots_as_tuples, (
          (15, ('A', 'B', 'C')),
          (10, ('B', 'C', 'A')),
          (8, ('C', 'B', 'A'))))
    self.assertEqual(test_tabulation.max_ranking_levels, 3)
    self.assertEqual(test_tabulation.tie_breaker, {'A': 0, 'B': 1, 'C': 2})
    self.assertEqual(test_tabulation.options,
          {'alternative_defeats': 'N'
            ,'type_of_altdefs': 'before_single_defeats'
            ,'always_count_votes': True
          })

  def test_meek_02(self):
    test_tabulation = self.make_meek_01()
    self.assertEqual(test_tabulation._other_categories(),
          {':Other exhausted': [], ':Abstentions': [], ':Overvotes': [],
          ':Votes for candidates': [], ':Total votes': [], ':Quota': [],
          ':Total surplus': [], ':Iterations': []})

  def test_meek_03(self):
    test_tabulation = self.make_meek_03()
    self.assertEqual(test_tabulation._other_categories(),
          {':Other exhausted': [], ':Abstentions': [], ':Overvotes': [],
          ':Votes for candidates': [], ':Total votes': [], ':Quota': [],
          ':Total surplus': [], ':Iterations': []})
    test_tabulation.tabulate(stop_after_tally=1)

  def test_distribute_votes(self):
    test_tabulation = self.make_meek_01()
    test_tabulation.tabulate(stop_at_begin=1)
    test_tabulation.nbr_iteration = 1
    test_tabulation._init_iter_tally()
    test_tabulation._distribute_votes()
    self.assertEqual(test_tabulation.iter_tally['A'], K.ONE * 15)
    self.assertEqual(test_tabulation.iter_tally['B'], K.ONE * 10)

  def test_update_candidate_status_tally(self):
    test_tabulation = self.make_meek_01()
    test_tabulation.tabulate(stop_at_begin=1)
    self.assertEqual(test_tabulation.status['B'].votes, None)
    self.assertEqual(test_tabulation.status['B'].nbr_round, 0)
    self.assertEqual(test_tabulation.status['B'].destiny, 'normal')
    test_tabulation.nbr_iteration = 1
    test_tabulation._init_iter_tally()
    test_tabulation._distribute_votes()
    test_tabulation._update_candidate_status_tally()
    self.assertEqual(test_tabulation.status['B'].votes, K.ONE * 10)
    self.assertEqual(test_tabulation.status['B'].nbr_round, 1)
    self.assertEqual(test_tabulation.status['B'].destiny, 'normal')

  def test_defeat_candidate(self):
    test_tabulation = self.make_meek_01()
    test_tabulation.tabulate(stop_after_status_update=1)
    self.assertEqual(test_tabulation.status['B'].status, K.STATUS_HOPEFUL)
    self.assertEqual(test_tabulation._hopeful(), set(str_tuple(' A B C')))
    self.assertEqual(test_tabulation._elected(), set())
    test_tabulation._defeat_candidates(set(['B']))
    self.assertEqual(test_tabulation.status['B'].status, K.STATUS_DEFEATED)
    self.assertEqual(test_tabulation._hopeful(), set(str_tuple(' A C')))
    self.assertEqual(test_tabulation._elected(), set())

  def test_elect_candidate(self):
    test_tabulation = self.make_meek_01()
    test_tabulation.tabulate(stop_after_status_update=1)
    self.assertEqual(test_tabulation.status['B'].status, K.STATUS_HOPEFUL)
    self.assertEqual(test_tabulation.status['C'].status, K.STATUS_HOPEFUL)
    self.assertEqual(test_tabulation._hopeful(), set(str_tuple(' A B C')))
    self.assertEqual(test_tabulation._elected(), set())
    test_tabulation._elect_candidates(set(['C', 'B']))
    self.assertEqual(test_tabulation.status['B'].status, K.STATUS_ELECTED)
    self.assertEqual(test_tabulation.status['C'].status, K.STATUS_ELECTED)
    self.assertEqual(test_tabulation._hopeful(), set(('A',)))
    self.assertEqual(test_tabulation._elected(), set(str_tuple(' B C')))

  def test_resolve_tie_1(self):
    test_tabulation = self.make_meek_01()
    test_tabulation.tabulate(stop_after_status_update=1)
    self.assertEqual(test_tabulation.tie_breaker, {'A':0, 'B':1, 'C':2})
    self.assertEqual(test_tabulation._resolve_tie(set(str_tuple(' A B C'))),
          'A')
    self.assertEqual(test_tabulation._resolve_tie(set(str_tuple(' B C'))), 'B')

  def test_get_single_defeat_candidate_1(self):
    test_tabulation = self.make_meek_01()
    test_tabulation.tabulate(stop_after_status_update=1)
    test_tabulation.total_surplus = test_tabulation._get_total_surplus()
    self.assertEqual(test_tabulation._get_single_defeat_candidate(),
          set(('C',)))
    test_tabulation.status['A'].votes = K.ONE * 8
    self.assertEqual(test_tabulation._get_single_defeat_candidate(),
          set(('A',)))


