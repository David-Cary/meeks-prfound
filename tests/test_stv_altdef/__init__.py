# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0
"""
Multi-winner file-specified tests for alternative defeats

Multi-winner alternative defeats include one or both of:

  - defeating more than one candidate in a round
  - defeating one or more candidates when an elected candidate still has
    surplus that has not been transferred, also known as deferred
    surplus transfers.
"""
