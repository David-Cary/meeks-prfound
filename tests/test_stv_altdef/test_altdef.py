# -*- encoding=utf-8 -*-
# Copyright 2016-2019 David Cary; licensed under the Apache License,
#       Version 2.0

import unittest
import _test_from_file


class TestAltDef(unittest.TestCase):
  """Test STV from file-based specs for alternative defeats"""

  def setUp(self):
    self.save_maxDiff = self.maxDiff
    self.maxDiff = 800

  def test_altdef_000_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-000-1.json')

  def test_altdef_000_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-000-2.json')

  def test_altdef_000_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-000-prr.json')

  def test_altdef_000_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-000-bsd.json')

  def test_altdef_000_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-000-inne.json')

  def test_altdef_001_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-001-1.json')

  def test_altdef_001_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-001-2.json')

  def test_altdef_001_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-001-prr.json')

  def test_altdef_001_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-001-bsd.json')

  def test_altdef_001_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-001-inne.json')

  def test_altdef_002_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-002-1.json')

  def test_altdef_002_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-002-2.json')

  def test_altdef_002_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-002-prr.json')

  def test_altdef_002_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-002-bsd.json')

  def test_altdef_002_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-002-inne.json')

  def test_altdef_003_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-003-1.json')

  def test_altdef_003_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-003-2.json')

  def test_altdef_003_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-003-prr.json')

  def test_altdef_003_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-003-bsd.json')

  def test_altdef_003_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-003-inne.json')

  def test_altdef_004_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-004-1.json')

  def test_altdef_004_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-004-2.json')

  def test_altdef_004_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-004-prr.json')

  def test_altdef_004_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-004-bsd.json')

  def test_altdef_004_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-004-inne.json')

  def test_altdef_005_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-005-1.json')

  def test_altdef_005_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-005-2.json')

  def test_altdef_005_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-005-prr.json')

  def test_altdef_005_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-005-bsd.json')

  def test_altdef_005_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-005-inne.json')

  def test_altdef_011_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-011-1.json')

  def test_altdef_011_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-011-2.json')

  def test_altdef_012_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-012-1.json')

  def test_altdef_012_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-012-2.json')

  def test_altdef_013_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-013-1.json')

  def test_altdef_013_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-013-2.json')

  def test_altdef_013_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-013-prr.json')

  def test_altdef_013_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-013-bsd.json')

  def test_altdef_013_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-013-inne.json')

  def test_altdef_014_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-014-1.json')

  def test_altdef_014_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-014-2.json')

  def test_altdef_014_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-014-prr.json')

  def test_altdef_014_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-014-bsd.json')

  def test_altdef_014_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-014-inne.json')

  def test_altdef_015_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-015-1.json')

  def test_altdef_015_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-015-2.json')

  def test_altdef_015_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-015-prr.json')

  def test_altdef_015_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-015-bsd.json')

  def test_altdef_015_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-015-inne.json')

  def test_altdef_016_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-016-1.json')

  def test_altdef_016_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-016-2.json')

  def test_altdef_016_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-016-prr.json')

  def test_altdef_016_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-016-bsd.json')

  def test_altdef_016_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-016-inne.json')

  def test_altdef_017_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-017-1.json')

  def test_altdef_017_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-017-2.json')

  def test_altdef_017_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-017-prr.json')

  def test_altdef_017_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-017-bsd.json')

  def test_altdef_017_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-017-inne.json')

  # for test conditions for test_altdef_018*, see altdef test 13

  def test_altdef_019_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-019-1.json')

  def test_altdef_019_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-019-2.json')

  def test_altdef_019_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-019-prr.json')

  def test_altdef_019_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-019-bsd.json')

  def test_altdef_019_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-019-inne.json')

  def test_altdef_020_1(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-020-1.json')

  def test_altdef_020_2(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-020-2.json')

  def test_altdef_020_prr(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-020-prr.json')

  def test_altdef_020_bsd(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-020-bsd.json')

  def test_altdef_020_inne(self):
    _test_from_file.run_test_spec(self, 'test_stv_altdef/altdef-020-inne.json')

